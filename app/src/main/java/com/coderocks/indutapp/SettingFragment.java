package com.coderocks.indutapp;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Checkable;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by watcharatepinkong on 5/16/16 AD.
 */
public class SettingFragment extends Fragment  implements Validator.ValidationListener {

    /**
     * Create a new instance of the fragment
     */

    public Validator  validator;


    @NotEmpty
   // @Email( message = "Please Check and Enter a valid Email Address")
    public EditText username;

    @Password(min = 4)
    public EditText password;


    public TextView displayname;

    public TextView displayemail;

    public RelativeLayout rev1;
    public RelativeLayout rev2;
    public LinearLayout rev3;
    public com.github.glomadrian.loadingballs.BallView ballview;

    public static SettingFragment newInstance(int index) {
        SettingFragment fragment = new SettingFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.ticket_setting, container, false);


        SharedPreferences sp = getActivity().getSharedPreferences("indy", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("login",0);
        editor.commit();


        rev3 = (LinearLayout) view.findViewById(R.id.rev3);
        rev1 = (RelativeLayout) view.findViewById(R.id.rev1);
        rev2 = (RelativeLayout) view.findViewById(R.id.rev2);

        ballview = (com.github.glomadrian.loadingballs.BallView)view.findViewById(R.id.ballView);

        ballview.setVisibility(View.INVISIBLE);
        rev2.setVisibility(View.GONE);

        username = (EditText) view.findViewById(R.id.username);
        password = (EditText) view.findViewById(R.id.password);


        displayname  = (TextView) view.findViewById(R.id.displayname);

        displayemail  = (TextView) view.findViewById(R.id.displayemail);

        ImageView signup = (ImageView)view.findViewById(R.id.signup);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validator.validate();
                // Intent intent = new Intent(getActivity(),SignupActivity.class);
                //startActivity(intent);

            }
        });

        ImageView signupbtn = (ImageView)view.findViewById(R.id.signupbtn);
        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // validator.validate();
                Intent intent = new Intent(getActivity(),SignupActivity.class);
                startActivity(intent);

            }
        });


       /* ImageView map = (ImageView)view.findViewById(R.id.ticket_past1);

        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                final AlertDialog dialog = builder.create();
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.testticket, null);
                dialog.setView(dialogLayout);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                dialog.show();

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface d) {



                    }
                });



            }
        });*/





        validator = new Validator(this);
        validator.setValidationListener(this);


         sp = getActivity().getSharedPreferences("indy", Context.MODE_PRIVATE);
        int login = sp.getInt("login",0);

        if(login == 1){
            rev1.setVisibility(View.GONE);
            rev2.setVisibility(View.VISIBLE);

            String username = sp.getString("displayname","");
            String email = sp.getString("displayemail","");

            for (int i = 0; i < 10; i++) {
                /*ImageView imageView = new ImageView(getActivity());
                imageView.setId(i);
                imageView.setImageBitmap(BitmapFactory.decodeResource(
                        getResources(), R.drawable.ticket_past));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
              //  params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                //params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);

              //  imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                rev3.addView(imageView,params);*/


                RelativeLayout relativeLayout = new RelativeLayout(getActivity());

                ImageView imageView = new ImageView(getActivity());
                imageView.setId(i);
                //imageView.setBackgroundColor(Color.BLACK);
                imageView.setImageBitmap(BitmapFactory.decodeResource(
                        getResources(), R.drawable.ticket_past));

                LinearLayout.LayoutParams b = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                imageView.setScaleType(ImageView.ScaleType.FIT_XY);

                imageView.setLayoutParams(b);


                relativeLayout.addView(imageView);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                rev3.addView(relativeLayout,params);

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(),ViewTicket.class);
                        startActivity(intent);

                    }
                });



            }

        }

        return view;

    }


    @Override
    public void onValidationSucceeded() {


        ballview.setVisibility(View.VISIBLE);
        Ion.with(getActivity())
                .load("POST","http://ticketmhee.com/api/index.php/api/login")
                .setBodyParameter("username", username.getText().toString())
                .setBodyParameter("password", password.getText().toString())

                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        ballview.setVisibility(View.INVISIBLE);





                        if(result.get("success").getAsBoolean()){

                            JsonObject obj = result.get("data").getAsJsonObject();
                            Log.d("dev","result===>"+obj.get("id"));
                            SharedPreferences sp = getActivity().getSharedPreferences("indy", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putInt("login",1);
                            editor.putInt("id",obj.get("id").getAsInt());
                            editor.putString("savename",obj.get("username").getAsString());
                            editor.putString("saveemail",obj.get("email").getAsString());

                            editor.commit();
                            displayname.setText(obj.get("username").getAsString());
                            displayemail.setText(obj.get("email").getAsString());


                            rev1.setVisibility(View.GONE);
                            rev2.setVisibility(View.VISIBLE);

                           // displayname.setText(result.get("username").toString());
                           // displayemail.setText(result.get("email").toString());


                            for (int i = 0; i < 10; i++) {
                                ImageView imageView = new ImageView(getActivity());
                                imageView.setId(i);
                                imageView.setImageBitmap(BitmapFactory.decodeResource(
                                        getResources(), R.drawable.ticket_past));
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                                rev3.addView(imageView);
                            }


                        }else{

                            Toast.makeText(getActivity(), "Login Faild!", Toast.LENGTH_LONG).show();
                        }
                        // JsonObject obj = result.getAsJsonObject("success");
                        /*if(!result.isJsonNull()) {
                            if (result.get("").getAsBoolean()) {

                            }else{




                            }
                        }*/


                    }
                });
       // Toast.makeText(getActivity(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
