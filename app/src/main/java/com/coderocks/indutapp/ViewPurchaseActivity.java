package com.coderocks.indutapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.concurrent.TimeUnit;

import co.omise.android.models.Token;
import co.omise.android.ui.CreditCardActivity;

/**
 * Created by watcharatepinkong on 5/17/16 AD.
 */
public class ViewPurchaseActivity extends AppCompatActivity implements Validator.ValidationListener {


    public String data1;
    public String data2;
    public String data3;
    public String data4;
    public String data5;


    public String id1;
    public String id2;
    public String id3;
    public String id4;
    public String id5;

    private int last = 1;
    private int pay = 0;


    TextView type1 ;
    TextView cost1 ;
    TextView qty1  ;
    TextView total1 ;



    TextView type2 ;
    TextView cost2 ;
    TextView qty2  ;
    TextView total2 ;


    TextView type3 ;
    TextView cost3 ;
    TextView qty3  ;
    TextView total3 ;


    TextView type4 ;
    TextView cost4 ;
    TextView qty4  ;
    TextView total4 ;

    TextView type5 ;
    TextView cost5 ;
    TextView qty5  ;
    TextView total5 ;


    @NotEmpty
    public EditText input_firstname;

    @NotEmpty
    public EditText input_lastname;

    @NotEmpty
    @Email( message = "Please Check and Enter a valid Email Address")
    public EditText input_email;

    Validator validator;


    public ImageView choosebankselect;
    public ImageView complete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchaselayout);




        input_firstname = (EditText) findViewById(R.id.input_firstname);
        input_lastname = (EditText) findViewById(R.id.input_lastname);
        input_email = (EditText) findViewById(R.id.input_email);


        Bundle bundle = getIntent().getExtras();

       // ArrayList<NameValuePair> nameValuePairs = (ArrayList<NameValuePair>) bundle.getSerializable("allorder");

        final String text = bundle.getString("title");
        final String id = bundle.getString("id");
        final Integer image = bundle.getInt("image");
        final String time = bundle.getString("time");

        final String path = bundle.getString("path");

        final String price = bundle.getString("price");
        final String qty = bundle.getString("qty");


         type1 = (TextView)findViewById(R.id.type1);
         cost1 = (TextView)findViewById(R.id.cost1);
         qty1 = (TextView)findViewById(R.id.qty1);
         total1 = (TextView)findViewById(R.id.total1);

         type2 = (TextView)findViewById(R.id.type2);
         cost2 = (TextView)findViewById(R.id.cost2);
         qty2 = (TextView)findViewById(R.id.qty2);
         total2 = (TextView)findViewById(R.id.total2);



        type3 = (TextView)findViewById(R.id.type3);
        cost3 = (TextView)findViewById(R.id.cost3);
        qty3 = (TextView)findViewById(R.id.qty3);
        total3 = (TextView)findViewById(R.id.total3);


        type4 = (TextView)findViewById(R.id.type4);
        cost4 = (TextView)findViewById(R.id.cost4);
        qty4 = (TextView)findViewById(R.id.qty4);
        total4 = (TextView)findViewById(R.id.total4);


        type5 = (TextView)findViewById(R.id.type5);
        cost5 = (TextView)findViewById(R.id.cost5);
        qty5 = (TextView)findViewById(R.id.qty5);
        total5 = (TextView)findViewById(R.id.total5);


        complete = (ImageView)findViewById(R.id.complete);

        type1.setVisibility(View.GONE);
        cost1.setVisibility(View.GONE);
        qty1.setVisibility(View.GONE);
        total1.setVisibility(View.GONE);


        type2.setVisibility(View.GONE);
        cost2.setVisibility(View.GONE);
        qty2.setVisibility(View.GONE);
        total2.setVisibility(View.GONE);


        type3.setVisibility(View.GONE);
        cost3.setVisibility(View.GONE);
        qty3.setVisibility(View.GONE);
        total3.setVisibility(View.GONE);


        type4.setVisibility(View.GONE);
        cost4.setVisibility(View.GONE);
        qty4.setVisibility(View.GONE);
        total4.setVisibility(View.GONE);


        type5.setVisibility(View.GONE);
        cost5.setVisibility(View.GONE);
        qty5.setVisibility(View.GONE);
        total5.setVisibility(View.GONE);


        try {
            data1 = bundle.getString("data1");
            String[] str = data1.split(",");


            showtext(last , str[0] , str[1] , str[2] , str[3]);

        }catch(Exception e){


        }


        try {
            data2 = bundle.getString("data2");
            String[] str = data2.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }



        try {
            data3 = bundle.getString("data3");
            String[] str = data3.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }


        try {
            data4 = bundle.getString("data4");
            String[] str = data4.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }


        try {
            data5 = bundle.getString("data5");
            String[] str = data5.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        toolbar.setBackgroundResource(R.drawable.bar_top);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText(text);
        getSupportActionBar().setCustomView(viewActionBar, params1);

      /*  ImageView choosebank = (ImageView)findViewById(R.id.choosebank);


        choosebank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), ChooseBankActivity.class);

                startActivity(intent);
                finish();
            }
        });*/


        final TextView _tv = (TextView) findViewById( R.id.ordernumber);
        new CountDownTimer(16*60000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                _tv.setText(""+String.format("เวลาที่เหลือ %d นาที %d วินาที",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
               finish();
            }
        }.start();

        ImageView complete = (ImageView)findViewById(R.id.complete);


        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), WaitingPaymentActivity.class);

                startActivity(intent);
                finish();
            }
        });

        ImageView eventimg = (ImageView)findViewById(R.id.slip);
        if(path !=null ){
            Ion.with(eventimg)
                    .placeholder(R.drawable.event_photo)
                    .error(R.drawable.event_photo)
                    .load(path);

        }



        complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                validator.validate();

            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);

        final ImageView  pay1 = (ImageView)findViewById(R.id.pay1);
        final ImageView  pay2 = (ImageView)findViewById(R.id.pay2);


//        LinearLayout form3 = (LinearLayout)findViewById(R.id.form3);

        choosebankselect = (ImageView)findViewById(R.id.choosebankselect);

    //    form3.setVisibility(View.GONE);
        choosebankselect.setVisibility(View.GONE);
        pay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosebankselect.setVisibility(View.GONE);
            if(pay==0){

                pay1.setImageResource(R.drawable.icon_credit_select);
                pay2.setImageResource(R.drawable.icon_bank);
                pay = 1;
            }else if(pay==1){
                pay1.setImageResource(R.drawable.icon_credit);
                pay = 0;
            }else if(pay==2){
                pay1.setImageResource(R.drawable.icon_credit_select);
                pay2.setImageResource(R.drawable.icon_bank);
                pay = 1;
            }

            }
        });


        pay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(pay==0){
                    pay2.setImageResource(R.drawable.icon_bank_select);
                    pay1.setImageResource(R.drawable.icon_credit);
                    pay = 2;
                }else if(pay==1){
                    pay2.setImageResource(R.drawable.icon_bank_select);
                    pay1.setImageResource(R.drawable.icon_credit);
                    pay = 2;
                }else if(pay==2){
                    pay2.setImageResource(R.drawable.icon_bank_select);
                    pay = 2;
                }

                Intent intent = new Intent(getBaseContext(), ChooseBankActivity.class);

                startActivityForResult(intent,1);


            }
        });


/*
        for(int i =0;i<=nameValuePairs.size();i++){


        }*/


/*



        eventimg.setImageResource(image);

        TextView qtylabel = (TextView)findViewById(R.id.qty);

        TextView pricelabel = (TextView)findViewById(R.id.price);

        if(price.equals("0")){
            qtylabel.setText(qty);
            pricelabel.setText("FREE");
        }else{
            qtylabel.setText(qty);
            pricelabel.setText(price);
        }

        final TextView qty1 = (TextView)findViewById(R.id.qty);




        ImageView testorder = (ImageView)findViewById(R.id.testpurchase2);

        testorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), ViewPurchaseResultActivity.class);

                intent.putExtra("title",text);
                intent.putExtra("id",id);
                intent.putExtra("image",image);
                intent.putExtra("time",time);
                intent.putExtra("price",price);
                intent.putExtra("qty",qty);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
                finish();
            }
        });*/
    }

    public  void showtext(int type , String s1 , String s2 , String s3, String s4 ){
        if(type == 1){

             type1.setText(s1);
             cost1.setText(s2);
             qty1.setText(s3);

            id1 = s4;

            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);

            type1.setVisibility(View.VISIBLE);
            cost1.setVisibility(View.VISIBLE);
            qty1.setVisibility(View.VISIBLE);
            total1.setVisibility(View.VISIBLE);

             total1.setText(""+tmp);

        }else if(type == 2){

            type2.setText(s1);
            cost2.setText(s2);
            qty2.setText(s3);
           // total2.setText(s4);

            id2 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type2.setVisibility(View.VISIBLE);
            cost2.setVisibility(View.VISIBLE);
            qty2.setVisibility(View.VISIBLE);
            total2.setVisibility(View.VISIBLE);
            total2.setText(""+tmp);

        }else if(type == 3){

            type3.setText(s1);
            cost3.setText(s2);
            qty3.setText(s3);
            // total2.setText(s4);

            id3 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type3.setVisibility(View.VISIBLE);
            cost3.setVisibility(View.VISIBLE);
            qty3.setVisibility(View.VISIBLE);
            total3.setVisibility(View.VISIBLE);
            total3.setText(""+tmp);

        }else if(type == 4){

            type4.setText(s1);
            cost4.setText(s2);
            qty4.setText(s3);
            // total2.setText(s4);

            id4 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type4.setVisibility(View.VISIBLE);
            cost4.setVisibility(View.VISIBLE);
            qty4.setVisibility(View.VISIBLE);
            total4.setVisibility(View.VISIBLE);
            total4.setText(""+tmp);

        }else if(type == 5){

            type5.setText(s1);
            cost5.setText(s2);
            qty5.setText(s3);
            // total2.setText(s4);

            id5 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type5.setVisibility(View.VISIBLE);
            cost5.setVisibility(View.VISIBLE);
            qty5.setVisibility(View.VISIBLE);
            total5.setVisibility(View.VISIBLE);
            total5.setText(""+tmp);

        }

        last = type +1;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        switch (requestCode) {
            case REQUEST_CC:
                if (resultCode == CreditCardActivity.RESULT_CANCEL) {
                    return;
                }

                Token token = data.getParcelableExtra(CreditCardActivity.EXTRA_TOKEN_OBJECT);


                Intent intent = new Intent(this, ReceiptActivity.class);
                //intent.putExtra(ReceiptActivity.EXTRA_PRODUCT_ID, productId());
                intent.putExtra(ReceiptActivity.EXTRA_TOKEN, token);


                intent.putExtra("data1", data1);
                intent.putExtra("data2", data2);
                intent.putExtra("data3", data3);
                intent.putExtra("data4", data4);
                intent.putExtra("data5", data5);


                startActivity(intent);
                finish();

                // process your token here.

            default:
                super.onActivityResult(requestCode, resultCode, data);
        }


        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {
            if(resultCode == Activity.RESULT_OK){
                String result=data.getStringExtra("result");
                choosebankselect.setVisibility(View.VISIBLE);
                if(result.equals("1")){

                    choosebankselect.setImageResource(R.drawable.bank_scb);
                }else if(result.equals("2")){

                    choosebankselect.setImageResource(R.drawable.bank_bkb);
                }else if(result.equals("3")){

                    choosebankselect.setImageResource(R.drawable.bank_gsb);
                }else if(result.equals("4")){

                    choosebankselect.setImageResource(R.drawable.bank_ksb);
                }else if(result.equals("5")){

                    choosebankselect.setImageResource(R.drawable.bank_kskb);
                }else if(result.equals("6")){

                    choosebankselect.setImageResource(R.drawable.bank_ktb);
                }else if(result.equals("7")){

                    choosebankselect.setImageResource(R.drawable.bank_tmb);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }




    }


    private static final String OMISE_PKEY = "pkey_test_52p2h2h1ujsv0pf3ii2";
    private static final int REQUEST_CC = 100;

    private void showCreditCardForm() {
        Intent intent = new Intent(this, CreditCardActivity.class);
        intent.putExtra(CreditCardActivity.EXTRA_PKEY, OMISE_PKEY);
        startActivityForResult(intent, REQUEST_CC);
    }

    @Override
    public void onValidationSucceeded() {

        Log.d("dev","---->onValidationSucceeded");
        /*complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });*/


        if(pay == 1){
            showCreditCardForm();

        }else{

            Intent intent = new Intent(getBaseContext(), WaitingPaymentActivity.class);


            intent.putExtra("data1", data1);
            intent.putExtra("data2", data2);
            intent.putExtra("data3", data3);
            intent.putExtra("data4", data4);
            intent.putExtra("data5", data5);

            startActivity(intent);
            finish();

        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {


        Log.d("dev","---->onValidationFailed");
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getBaseContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}