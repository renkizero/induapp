package com.coderocks.indutapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.NameValuePair;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import me.himanshusoni.quantityview.QuantityView;

/**
 * Created by watcharatepinkong on 5/17/16 AD.
 */
public class ViewCategoryDetail extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,

        LocationListener {

    private GoogleMap mMap;
    public String namet1;
    public String namet2;
    public String namet3;
    public String namet4;
    public String namet5;


    public String data1;
    public String data2;
    public String data3;
    public String data4;
    public String data5;


    public String pdata1;
    public String pdata2;
    public String pdata3;
    public String pdata4;
    public String pdata5;

    public String id1;
    public String id2;
    public String id3;
    public String id4;
    public String id5;


    public String banner;

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    private Marker mBrisbane;
    public SharedPreferences sharedPref;

    public float lat,lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testdetail);


        Bundle bundle = getIntent().getExtras();
        final String text = bundle.getString("title");
        final String id = bundle.getString("id");
        final Integer image = bundle.getInt("image");
        final String time = bundle.getString("time");
        final String price = bundle.getString("price");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        String txt = "";


        final TextView title1 = (TextView) findViewById(R.id.ordernumber);
        title1.setText(text);
        title1.setVisibility(View.GONE);


        final WebView detail = (WebView) findViewById(R.id.detail);
        //  detail.setText(txt);


       /* TextView qty = (TextView)findViewById(R.id.qty);

        TextView pricelabel = (TextView)findViewById(R.id.price1);

        if(price.equals("0")){
            qty.setVisibility(View.GONE);
            pricelabel.setText("FREE");
        }else{
            qty.setVisibility(View.VISIBLE);
            pricelabel.setText(price);
        }*/
        final TextView location = (TextView) findViewById(R.id.location);


        final TextView datetime = (TextView) findViewById(R.id.datetime);
        // datetime.setText(time);


        final ImageView eventimg = (ImageView) findViewById(R.id.slip);
        final ImageView loct = (ImageView) findViewById(R.id.loct);

/*
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.testmap);


        mapFragment.getMapAsync(this);*/

        // eventimg.setImageResource(image);




        final com.github.glomadrian.loadingballs.BallView ballview = (com.github.glomadrian.loadingballs.BallView) findViewById(R.id.ballView);

        Log.d("dev", "id==>" + id);
        final String begin = " <html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/font/Nunito-Light.ttf\")}body {font-family: MyFont;font-size: x-small;text-align: center;}</style></head><body>";
        final String end = "</body></html>";
        Ion.with(getBaseContext())
                .load("POST", "http://ticketmhee.com/api/index.php/api/getEventByID")
                .setBodyParameter("id", id)
                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        ballview.setVisibility(View.INVISIBLE);


                        JsonArray obj = result.getAsJsonArray("data");

                        for (int i = 0; i < obj.size(); i++) {
                            JsonObject news = obj.get(i).getAsJsonObject();
                            title1.setText(news.get("name").getAsString());
                            detail.loadDataWithBaseURL("", begin + news.get("desc").getAsString() + end, "text/html", "UTF-8", "");
                            if (!news.get("path").isJsonNull()) {
                                Ion.with(eventimg)
                                        .placeholder(R.drawable.event_photo)
                                        .error(R.drawable.event_photo)
                                        .load(news.get("path").getAsString());

                                banner = news.get("path").getAsString();
                            }

                            datetime.setText(news.get("start_date").getAsString() + " " + news.get("start_time").getAsString() + " - " + news.get("end_date").getAsString() + " " + news.get("end_time").getAsString());
                            location.setText(news.get("place").getAsString());
                            if (!news.get("latitude").isJsonNull()) {
                                lat = Float.parseFloat(news.get("latitude").getAsString());
                                //  mapFragment.getActivity().setVisibility(View.VISIBLE);
                            }

                            if (!news.get("longitude").isJsonNull()) {
                                lon = Float.parseFloat(news.get("longitude").getAsString());
                                //  mapFragment.getActivity().setVisibility(View.VISIBLE);
                            }

                        }

                    }
                });


        final TextView price1 = (TextView) findViewById(R.id.price1);
        final TextView price2 = (TextView) findViewById(R.id.price2);
        final TextView price3 = (TextView) findViewById(R.id.price3);
        final TextView price4 = (TextView) findViewById(R.id.price4);
        final TextView price5 = (TextView) findViewById(R.id.price5);

        final QuantityView qty1 = (QuantityView) findViewById(R.id.qty1);
        final QuantityView qty2 = (QuantityView) findViewById(R.id.qty2);
        final QuantityView qty3 = (QuantityView) findViewById(R.id.qty3);
        final QuantityView qty4 = (QuantityView) findViewById(R.id.qty4);
        final QuantityView qty5 = (QuantityView) findViewById(R.id.qty5);

        final WebView detailevent = (WebView) findViewById(R.id.detailevent);

        // datetime.setText(time);

        price1.setVisibility(View.GONE);
        price2.setVisibility(View.GONE);
        price3.setVisibility(View.GONE);
        price4.setVisibility(View.GONE);
        price5.setVisibility(View.GONE);

        qty1.setVisibility(View.GONE);
        qty2.setVisibility(View.GONE);
        qty3.setVisibility(View.GONE);
        qty4.setVisibility(View.GONE);
        qty5.setVisibility(View.GONE);


        Ion.with(getBaseContext())
                .load("POST", "http://ticketmhee.com/api/index.php/api/getTicketAllByEventID")
                .setBodyParameter("id", id)
                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error


                        try {
                            Log.d("dev", "==>" + result);

                            JsonArray obj = result.getAsJsonArray("data");

                            for (int i = 0; i < obj.size(); i++) {
                                JsonObject news = obj.get(i).getAsJsonObject();

                                if (i == 0) {
                                    id1 = news.get("id").getAsString();
                                    pdata1 = news.get("price").getAsString();
                                    namet1 = news.get("name").getAsString();
                                    price1.setText(news.get("price").getAsString() + " บาท");
                                    price1.setVisibility(View.VISIBLE);
                                    qty1.setVisibility(View.VISIBLE);
                                    qty1.setMaxQuantity(Integer.parseInt(news.get("max_number").getAsString()));
                                } else if (i == 1) {
                                    id2 = news.get("id").getAsString();
                                    pdata2 = news.get("price").getAsString();
                                    namet2 = news.get("name").getAsString();
                                    price2.setVisibility(View.VISIBLE);
                                    price2.setText(news.get("price").getAsString() + " บาท");

                                    qty2.setVisibility(View.VISIBLE);
                                    qty2.setMaxQuantity(Integer.parseInt(news.get("max_number").getAsString()));

                                } else if (i == 2) {
                                    id3 = news.get("id").getAsString();
                                    pdata3 = news.get("price").getAsString();
                                    namet3 = news.get("name").getAsString();
                                    price3.setVisibility(View.VISIBLE);
                                    price3.setText(news.get("price").getAsString() + " บาท");

                                    qty3.setVisibility(View.VISIBLE);
                                    qty3.setMaxQuantity(Integer.parseInt(news.get("max_number").getAsString()));


                                } else if (i == 3) {
                                    id4 = news.get("id").getAsString();
                                    pdata4 = news.get("price").getAsString();
                                    namet4 = news.get("name").getAsString();
                                    price4.setVisibility(View.VISIBLE);
                                    price4.setText(news.get("price").getAsString() + " บาท");

                                    qty4.setVisibility(View.VISIBLE);
                                    qty4.setMaxQuantity(Integer.parseInt(news.get("max_number").getAsString()));

                                } else if (i == 4) {
                                    id5 = news.get("id").getAsString();
                                    pdata5 = news.get("price").getAsString();
                                    namet5 = news.get("name").getAsString();
                                    price5.setVisibility(View.VISIBLE);
                                    price5.setText(news.get("price").getAsString() + " บาท");

                                    qty5.setVisibility(View.VISIBLE);
                                    qty5.setMaxQuantity(Integer.parseInt(news.get("max_number").getAsString()));

                                }
                                detailevent.loadDataWithBaseURL("", begin + news.get("desc").getAsString() + end, "text/html", "UTF-8", "");
                            }
                        } catch (Exception e1) {

                        }


                    }
                });


        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions(getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        toolbar.setBackgroundResource(R.drawable.bar_top);


        ImageView reservbtn = (ImageView) findViewById(R.id.reservbtn);


        // final EditText qtyedit =  (EditText)findViewById(R.id.qty);
        reservbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            /*
            *
            *   String text = bundle.getString("title");
        String id = bundle.getString("id");
        Integer image = bundle.getInt("image");
        String time = bundle.getString("time");
        String price = bundle.getString("price");
            * */
                int total = qty1.getQuantity() + qty2.getQuantity() + qty3.getQuantity() + qty4.getQuantity() + qty5.getQuantity();
                if (total > 0) {


                    SharedPreferences sp = getSharedPreferences("indy", Context.MODE_PRIVATE);
                    int login = sp.getInt("login",0);

                    Intent intent = new Intent(getBaseContext(), ViewPurchaseActivity.class);
                    if(login == 1){

                        intent.putExtra("title", text);
                        intent.putExtra("id", id);
                        intent.putExtra("image", image);
                        intent.putExtra("time", time);
                        intent.putExtra("price", price);
                        intent.putExtra("path", banner);


                        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();


                        if (qty1.getQuantity() > 0) {
                            data1 = namet1 + "," + pdata1 + "," + qty1.getQuantity() + "," + id1;
                            //   nameValuePairs.add(new BasicNameValuePair("order", namet1+"|"+qty1.getQuantity()));
                        }

                        if (qty2.getQuantity() > 0) {
                            data2 = namet2 + "," + pdata2 + "," + qty2.getQuantity() + "," + id2;
                            //  nameValuePairs.add(new BasicNameValuePair("order", namet2+"|"+qty2.getQuantity()));
                        }

                        if (qty3.getQuantity() > 0) {
                            data3 = namet3 + "," + pdata3 + "," + qty3.getQuantity() + "," + id3;
                            // nameValuePairs.add(new BasicNameValuePair("order", namet3+"|"+qty3.getQuantity()));
                        }

                        if (qty4.getQuantity() > 0) {
                            data4 = namet4 + "," + pdata4 + "," + qty4.getQuantity() + "," + id4;
                            // nameValuePairs.add(new BasicNameValuePair("order", namet4+"|"+qty4.getQuantity()));
                        }

                        if (qty5.getQuantity() > 0) {
                            data5 = namet5 + "," + pdata5 + "," + qty5.getQuantity() + "," + id5;
                            //  nameValuePairs.add(new BasicNameValuePair("order", namet5+"|"+qty5.getQuantity()));
                        }

                        //Bundle extra1 = new Bundle();
                        //extra1.putSerializable("allorder",nameValuePairs);
                        // intent.putExtra(extra1);
                        //intent.putStringArrayListExtra("allorder",nameValuePairs);
                        //    intent.putExtra("allorder",nameValuePairs);


                        intent.putExtra("data1", data1);
                        intent.putExtra("data2", data2);
                        intent.putExtra("data3", data3);
                        intent.putExtra("data4", data4);
                        intent.putExtra("data5", data5);


                        intent.putExtra("qty1", qty1.getQuantity());
                        intent.putExtra("qty2", qty2.getQuantity());
                        intent.putExtra("qty3", qty3.getQuantity());
                        intent.putExtra("qty4", qty4.getQuantity());
                        intent.putExtra("qty5", qty5.getQuantity());


                        //intent.putExtra("qty",qtyedit.getText().toString());
                        //String message = "abc";
                        //intent.putExtra(EXTRA_MESSAGE, message);
                        startActivity(intent);

                    }else{
                        Toast.makeText(getBaseContext(), "Please Login!", Toast.LENGTH_LONG).show();


                    }



                }

            }
        });


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText(text);
        getSupportActionBar().setCustomView(viewActionBar, params1);

        /*ImageView testorder = (ImageView)findViewById(R.id.testorder);

        testorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), ViewPurchaseActivity.class);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }




    public GoogleMap.OnCameraChangeListener getCameraChangeListener()
    {
        return new GoogleMap.OnCameraChangeListener()
        {
            @Override
            public void onCameraChange(CameraPosition position)
            {
                //Log.d("Zoom", "Zoom: " + position.zoom);
                //Log.d("Zoom", "Zoom: " + position.target.latitude);
                //Log.d("Zoom", "Zoom: " + position.target.longitude);



            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private static final LatLng BRISBANE = new LatLng(13.5662986, 100.5297313);

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;


       // googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        //mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.setOnCameraChangeListener(getCameraChangeListener());
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.setOnCameraChangeListener(getCameraChangeListener());

        }


        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng arg0) {
                // TODO Auto-generated method stub
                //Log.d("arg0", arg0.latitude + "-" + arg0.longitude);
            }
        });


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        settingsrequest();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }


    public void settingsrequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(ViewCategoryDetail.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // startLocationUpdates();
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                mGoogleApiClient,
                                mLocationRequest,
                                this
                        ).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {

                            }
                        });
                        break;
                    case Activity.RESULT_CANCELED:
                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


        Log.d("dev","lat====>"+lat);
        //Place current location marker
        LatLng latLng = new LatLng(lat, lon);


        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        // mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }
}
