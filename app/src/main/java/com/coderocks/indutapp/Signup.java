package com.coderocks.indutapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

/**
 * Created by watcharatepinkong on 5/7/16 AD.
 */
public class Signup extends AppCompatActivity  implements Validator.ValidationListener {

    Validator validator;

    @NotEmpty
    @Email
    private EditText emailEditText;


    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS)
    private EditText passwd;

    @ConfirmPassword
    private EditText cpassword;


    @NotEmpty
    private EditText firstname;

    @NotEmpty
    private EditText lastname;

    private Drawer result;
    public Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);




        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Home");
        SecondaryDrawerItem item2 = (SecondaryDrawerItem) new SecondaryDrawerItem().withName("Category");

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        new SecondaryDrawerItem().withName("Order"),
                        new SecondaryDrawerItem().withName("Tickets"),
                        new SecondaryDrawerItem().withName("Sign In"),
                        new SecondaryDrawerItem().withName("Sign Up")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        result.closeDrawer();
                        switch (position){

                            case 1:
                                Intent intent = new Intent(Signup.this,MainActivity.class);
                                startActivity(intent);
                                break;
                            case 2:
                                intent = new Intent(Signup.this,CategoryActivity.class);
                                startActivity(intent);
                                break;

                            case 3:
                                intent = new Intent(Signup.this,ViewOrder.class);
                                startActivity(intent);
                                break;

                            case 4:
                                intent = new Intent(Signup.this,TicketActivity.class);
                                startActivity(intent);
                                break;

                            case 5:
                                intent = new Intent(Signup.this,LoginAcivity.class);
                                startActivity(intent);
                                break;
                            case 6:
                                intent = new Intent(Signup.this,Signup.class);
                                startActivity(intent);
                                break;
                        }
                        return true;
                    }
                })
                .build();


        validator = new Validator(this);
        validator.setValidationListener(this);


        emailEditText = (EditText) findViewById(R.id.username);
        passwd = (EditText) findViewById(R.id.passwd);
        cpassword = (EditText) findViewById(R.id.cpassword);
        firstname = (EditText) findViewById(R.id.firstname);

        lastname = (EditText) findViewById(R.id.lastname);
        submit = (Button) findViewById(R.id.submit);



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Signup.this,LoginAcivity.class);
                startActivity(intent);
                finish();

            }
        });

    }

    @Override
    public void onValidationSucceeded() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            finish();

            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }
}