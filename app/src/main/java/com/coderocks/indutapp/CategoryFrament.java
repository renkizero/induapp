package com.coderocks.indutapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

/**
 * Created by watcharatepinkong on 5/17/16 AD.
 */
public class CategoryFrament  extends Fragment {

    /**
     * Create a new instance of the fragment
     */
    public static CategoryFrament newInstance(int index) {
        CategoryFrament fragment = new CategoryFrament();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    //public static String [] prgmNameList={"ทั้งหมด","วิทยาศาสตร์","ครอบครัว","การศึกษา","ธุรกิจ","เพลง","สุขภาพ","การสื่อสาร","อาหารและเครื่องดื่ม","แฟชั่น","ศิลปะ","กีฬา","ความบันเทิง","งานอดิเรก","การกุศล"};
    //public static int [] prgmImages={R.drawable.icon_all,R.drawable.icon_sci,R.drawable.icon_family,R.drawable.icon_education,R.drawable.icon_business,R.drawable.icon_music,R.drawable.icon_health,R.drawable.icon_com,R.drawable.icon_food,R.drawable.icon_fashion,R.drawable.icon_art,R.drawable.icon_sport,R.drawable.icon_movie,R.drawable.icon_hobby,R.drawable.icon_charity};


    //public static String [] prgmNameList={"พลาสติก","ไฟฟ้าและอิเลกทรอนิกส์","ยานยนต์"};
    //public static int [] prgmImages={R.drawable.icon_education,R.drawable.icon_sci,R.drawable.icon_business};
    ArrayList<String> prgmNameList = new ArrayList<String>();
  //  public static String[] prgmNameList={"ทั้งหมด"};
    public static int[] prgmImages={R.drawable.icon_all_select,R.drawable.icon_atsi_select,R.drawable.icon_sme_select,R.drawable.icon_smebank_select,R.drawable.icon_thiti_select,R.drawable.icon_smecn_select,R.drawable.icon_tma_select,R.drawable.icon_composite_select,R.drawable.icon_bsid_select,R.drawable.icon_subcon_select};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayOptions( ((AppCompatActivity)getActivity()).getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = inflater.inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText("หมวดหมู่");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(viewActionBar, params1);


        View view = inflater.inflate(R.layout.viewcate, container, false);
        final GridView gridView = (GridView) view.findViewById(R.id.gridView1);

        prgmNameList.add("ทั้งหมด");
        final com.github.glomadrian.loadingballs.BallView ballview = (com.github.glomadrian.loadingballs.BallView)view.findViewById(R.id.ballView);

        Ion.with(getActivity())
                .load("http://ticketmhee.com/api/index.php/api/getCateAll")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error

                        Log.d("dev","==>"+result);
                        try {
                            if(!result.isJsonNull()){
                                ballview.setVisibility(View.INVISIBLE);
                                JsonArray obj = result.getAsJsonArray("data");
                               // int j = 1;
                                Log.d("dev","==>"+obj.size());
                                for (int i = 0; i < obj.size(); i++) {

                                    if(i>2){
                                        JsonObject news = obj.get(i).getAsJsonObject();
                                        Log.d("dev","==>"+news.get("name").getAsString().toString());
                                        //prgmNameList[] = ""+news.get("name").getAsString().toString();
                                        prgmNameList.add(news.get("name").getAsString().toString());
                                        Log.d("dev","==>");

                                    }
                                }
                                gridView.setAdapter(new CustomAdapter(getActivity().getBaseContext(), prgmNameList,prgmImages));

                                gridView.notifyAll();
                            }
                        }catch (Exception e1){


                        }



                    }
                });



        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Intent intent = new Intent(getActivity(), ViewCategoryActivity.class);
                intent.putExtra("gid",position);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });
        return  view;
    }


    public class CustomAdapter extends BaseAdapter {

        ArrayList<String> result;
        Context context;
        int[] imageId;
        private LayoutInflater inflater = null;

        public CustomAdapter(Context mainActivity, ArrayList<String> prgmNameList, int[] prgmImages) {
            // TODO Auto-generated constructor stub
            result = prgmNameList;
            context = mainActivity;
            imageId = prgmImages;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder {
            TextView tv;
            ImageView img;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder holder = new Holder();
            View rowView;

            rowView = inflater.inflate(R.layout.catelist, null);
            holder.tv = (TextView) rowView.findViewById(R.id.textView1);
            holder.img = (ImageView) rowView.findViewById(R.id.imageView1);

            holder.tv.setTextColor(Color.parseColor("#10579e"));
            holder.tv.setText(result.get(position));
            holder.img.setImageResource(imageId[position]);

            rowView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getActivity(), ViewCategoryActivity.class);
                    //String message = "abc";
                    //intent.putExtra(EXTRA_MESSAGE, message);
                    Log.d("","position===<>"+position);

                    intent.putExtra("gid",""+position);
                    startActivity(intent);
                    // TODO Auto-generated method stub
                    //  Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
                }
            });

            return rowView;
        }
    }
}
