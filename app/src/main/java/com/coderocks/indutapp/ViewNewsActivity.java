package com.coderocks.indutapp;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

/**
 * Created by watcharatep.i on 5/18/2016 AD.
 */
public class ViewNewsActivity  extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newsdetail);

        Bundle bundle = getIntent().getExtras();
        final String text = bundle.getString("title");
        final String id = bundle.getString("id");
        final Integer image = bundle.getInt("image");

            Log.d("dev","id====>"+id);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        String txt = "";




        final TextView title1 = (TextView)findViewById(R.id.ordernumber);
        title1.setText(text);



        final WebView detail = (WebView)findViewById(R.id.detail);
        detail.getSettings().setJavaScriptEnabled(true);


        final ImageView eventimg = (ImageView)findViewById(R.id.slip);

        eventimg.setImageResource(image);


        final com.github.glomadrian.loadingballs.BallView ballview = (com.github.glomadrian.loadingballs.BallView)findViewById(R.id.ballView);


        Ion.with(getBaseContext())
                .load("POST","http://ticketmhee.com/api/index.php/api/getNewsByID")
                .setBodyParameter("id", id)
                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        ballview.setVisibility(View.INVISIBLE);
                        Log.d("dev","==>"+result);

                        JsonArray obj = result.getAsJsonArray("data");

                        for (int i = 0; i < obj.size(); i++) {
                            JsonObject news = obj.get(i).getAsJsonObject();
                            title1.setText(news.get("title").getAsString());
                            detail.loadDataWithBaseURL("", news.get("desc").getAsString(), "text/html", "UTF-8", "");

                            try{
                                if(!news.get("path").isJsonNull()){
                                    Ion.with(eventimg)
                                            .placeholder(R.drawable.event_photo)
                                            .error(R.drawable.event_photo)
                                            .load(news.get("path").getAsString());
                                }
                            }catch (Exception e1){


                            }

                        }

                    }
                });


/*
        if(id.endsWith("1")){
            txt ="บริษัท ExxonMobil Chemical ได้เปิดตัวเม็ดพลาสติก PE ชนิด Metallocene (mPE) รุ่น Enable 40-02 ที่เหมาสำหรับการผลิตฟิล์มหด (shrink films) ที่มีคุณสมบัติที่แข็งแรงขึ้นและไม่ฉีกขาดง่าย ทำให้ผู้ผลิตสามารถผลิตฟิล์มที่บางลงซึ่งมีความแข็งแรงมากขึ้นเมื่อเทียบกับฟิล์มที่ผลิตจากเม็ด mPE ชนิดอื่นๆ ในปัจจุบัน \n" +
                    "\n" +
                    "ExxonMobil Chemical อ้างว่าเม็ด Enable 40-02 จะช่วยให้ผู้ผลิตสามารถลดต้นทุนและลดปริมาณเม็ดพลาสติกที่ใช้ลงได้กว่า 25% (เมื่อใช้เม็ด Enable 40-02 ในการผลิตฟิล์มหดแบบ 3 ชั้นที่มีความหนา 60 ไมครอน) Enable 40-02 ยังมีข้อดีอื่นๆ อีกเช่น มีความใสมากขึ้น ช่วยให้มองเห็นผลิตภัณฑ์ที่อยู่ภายในฟิล์มหดได้ชัดเจนมากขึ้น มีคุณสมบัติควบคุมทิศทางการหดตามขวางได้ดี (controlled transverse direction) ช่วยทำให้การหดของฟิล์มมีความสม่ำเสมอมากขึ้น และการที่ Enable 40-02 สามารถขึ้นรูปได้ง่ายสามารถช่วยให้ผู้ผลิตมีกระบวนการผลิตโดยรวมที่ดีขึ้นทั้งในด้านความเร็ว การลดปริมาณการใช้วัตถุดิบและการประหยัดพลังงานนั่นเอง\n" +
                    "\n" +
                    "ฟิล์มหดคุณภาพสูงที่ผลิตด้วย Enable 40-02 นี้เหมาะสำหรับใช้ในการบรรจุขวดเครื่องดื่ม กระป๋องอาหาร ผลิตภัณฑ์ของใช้ เครื่องสำอางและผลิตภัณฑ์อื่นๆ ที่ต้องการบรรจุภัณฑ์ที่แน่นหนาและสามารถปกป้องสินค้าได้ตั้งแต่ช่วงหลังจากการผลิตไปจนถึงมือผู้บริโภค (ตลอดทั้ง value chain)\n" +
                    "\n" +
                    "ที่มา: polymerupdate.com\n" +
                    "https://goo.gl/ACNIOb";


        }else if(id.endsWith("2")){
            txt ="ระบบเตือนภัยเศรษฐกิจอุตสาหกรรมไทยเดือนเมษายน 2559 ส่งสัญญาณเตือนระยะต้น อย่างไรก็ ตาม พบว่ามีตัวแปรชี้นาหลายตัวมีการปรับตัวลดลง ท้ังนี้ เน่ืองจากภาวะเศรษฐกิจของประเทศคู่ค้าและ เศรษฐกิจของไทย มีแนวโน้มชะลอตัวลง และยังไม่มีสัญญาณฟ้ืนตัวท่ีชัดเจน ทาให้คาดว่าในเดือน พฤษภาคม-มิถุนายน 2559 เศรษฐกิจอุตสาหกรรมไฟฟ้าและอิเล็กทรอนิกส์ไทยอาจมีแนวโน้มทรงตัวถึง อาจปรับตัวลดลงได้";
        }else if(id.endsWith("3")){

            txt = "รัฐมนตรีว่าการกระทรวงอุตสาหกรรม เป็นประธานงานแถลงข่าวโครงการยกระดับขีดความสามารถผู้ผลิตชิ้นส่วนยานยนต์ไทย\n" +
                    "04-05-2559\n" +
                    "เมื่อวันที่ 28 เมษายน 2559 นางอรรชกา สีบุญเรือง รัฐมนตรีว่าการกระทรวงอุตสาหกรรม เป็นประธานงานแถลงข่าวโครงการยกระดับขีดความสามารถผู้ผลิตชิ้นส่วนยานยนต์ไทย หรือ Original Design Manufacturer (ODM) จัดโดยสำนักงานเศรษฐกิจอุตสาหกรรม ร่วมกับบริษัท มาสด้า เซลส์ (ประเทศไทย) จำกัด โดยมีนายศิริรุจ จุลกะรัตน์ ผู้อำนวยการสำนักงานเศรษฐกิจอุตสาหกรรม และนายชาญชัย ตระการอุดมสุข รองประธานบริหารอาวุโส บริษัท มาสด้า เซลส์ (ประเทศไทย) จำกัด นางอัชณา ลิมป์ไพฑูรย์ นายกสมาคมผู้ผลิตชิ้นส่วนยานยนต์ไทย และนายกรกฤช จุฬางกูร ประธานบริหารบริษัท ซัมมิท ออโต้ชีท อินดัสตรี จำกัด ร่วมแถลงข่าว ณ ห้องประชุมชุณหะวัณ ชั้น 3 สำนักงานปลัดกระทรวงอุตสาหกรรม\n" +
                    " \tโครงการยกระดับขีดความสามารถผู้ผลิตชิ้นส่วนยานยนต์ไทย หรือ Original Design Manufacturer (ODM) เป็นโครงการที่ส่งวิศวกรจากบริษัทผู้ผลิตชิ้นส่วนยานยนต์ไทย ไปฝึกอบรมและร่วมทำงานกับวิศวกรชาวญี่ปุ่น ที่บริษัท มาสด้า คอร์ปอร์เรชั่น ประเทศญี่ปุ่น เพื่อให้วิศวกรชาวไทยได้มีโอกาสในการเรียนรู้เทคโนโลยี การออกแบบชิ้นส่วนยานยนต์ รวมถึงวิธีการทำงานในรูปแบบของวัฒนธรรมญี่ปุ่น เพื่อให้เกิดประสบการณ์ในการทำงานร่วมกันกับบริษัทผู้ผลิตรถยนต์ รวมถึงเกิดการสร้างเครือข่ายความร่วมมือระหว่างบริษัท นำไปสู่การพัฒนาชิ้นส่วนยานยนต์ร่วมกันในอนาคต โดยบริษัท มาสด้า เซลส์ (ประเทศไทย) จำกัด ได้คัดเลือกวิศวกรจากบริษัทผู้ผลิตชิ้นส่วนยานยนต์ไทย จำนวน 4 คน จากบริษัทไทยซัมมิท จำกัด จำนวน 2 คน จากบริษัท ซัมมิทออโต้บอดี้ จำนวน 1 คน และจากบริษัท ซัมมิท ออโต้ซีท จำนวน 1 คน เพื่อเข้าฝึกอบรมระหว่างวันที่ 9 พฤษภาคม - 12 กันยายน 2559 รวมระยะเวลา 4 เดือน";

        }else{

            txt = "มาสด้า เปิดบ้านรับวิศวกรไทย พัฒนาชิ้นส่วนยานยนต์สู่ตลาดโลก\n" +
                    "     \n" +
                    "          นายชาญชัย ตระการอุดมสุข รองประธานบริหาร บริษัท มาสด้า เซลส์ (ประเทศไทย) จำกัด ผู้จัดจำหน่ายรถยนต์มาสด้าในประเทศไทย เปิดเผยว่า มาสด้าได้ร่วมมือกับกระทรวงอุตสาห กรรม สมาคมผู้ผลิตชิ้นส่วนยานยนต์ไทย (TAPMA) มาสด้า มอเตอร์ คอร์ปอเรชั่น ประเทศญี่ปุ่น และบริษัทผู้ผลิตชิ้นส่วนยานยนต์ไทย บุกเบิกโครงการนำร่องเพื่อพัฒนาผู้ผลิตชิ้นส่วนไทย หรือ \"โครงการพัฒนาผู้ผลิตชิ้นส่วนยานยนต์ไทยไปสู่การผลิตตามรูปแบบของตนเอง หรือ Original Design Manufacturer (ODM)\" พร้อมยกระดับพัฒนาฝีมือผู้ผลิตชิ้นส่วนยานยนต์ไทยจากการผลิตตามแบบหรือ OEM สู่การสร้างนวัตกรรมริเริ่มการผลิตคิดค้นชิ้นส่วนในแบบของตนเองหรือ ODM\n" +
                    "          ตลอดระยะเวลากว่า 20 ปี ที่ผ่านมา มาสด้าได้แสดงถึงความเชื่อมั่นที่มีต่ออุตสาหกรรมยานยนต์ไทยมาโดยตลอด ไม่ว่าจะเป็นการก่อตั้งโรงงานผลิตรถยนต์ ภายใต้ชื่อ บริษัท ออโต้อัลลายแอนซ์ (ประเทศไทย) จำกัด  เพื่อทำการผลิตรถเพื่อการพาณิชย์และรถยนต์นั่งส่วนบุคคล เพื่อจำหน่ายในประเทศไทยและเพื่อการส่งออกกว่า 140 ประเทศทั่วโลก และเพิ่มเงินลงทุนอีกกว่า 1 หมื่นล้านบาท เพื่อเริ่มสายการผลิตรถยนต์มาสด้า 2 โฉมใหม่ ซึ่งเป็นรถยนต์ภายใต้โครงการรถยนต์ประหยัดพลังงานมาตรฐานสากล ระยะที่ 2 ส่งผลให้ประเทศไทยกลายเป็นศูนย์กลางการผลิตรถยนต์มาสด้าที่ใหญ่สุดในอาเซียน ล่าสุดมาสด้ายังทุ่มเม็ดเงินก้อนโตก่อตั้ง บริษัท มาสด้า พาวเวอร์เทรน แมนูแฟคเจอริ่งประเทศไทยฯ เพื่อทำการผลิตชุดเกียร์ออโตเมติก SKYACTIV-DRIVE ส่งออกสู่ประเทศต่างๆ ทั่วโลก\n" +
                    "          \"การริเริ่มดำเนินการโครงการในครั้งนี้ มีจุดมุ่งหมายเพื่อยกระดับขีดความสามารถของผู้ผลิตชิ้นส่วนยานยนต์ไทย โดยคณะผู้บริหารระดับสูงของ มาสด้า มอเตอร์ คอร์ปอเรชั่น ประเทศญี่ปุ่น ได้ให้ความสำคัญกับโครงการนี้เป็นอย่างมาก และมอบหมายให้ มาสด้าเซลส์ ประเทศไทย เป็นผู้พัฒนาและดำเนินการโครงการดังกล่าว ร่วมกับกระทรวงอุตสาหกรรม และบริษัทผู้ผลิตชิ้นส่วนยานยนต์ของไทย โดยทางมาสด้า มอเตอร์ ได้จัดเตรียมองค์ความรู้ สถานที่ทำงาน ตลอดจนผู้เชี่ยวชาญ เพื่อจะร่วมถ่ายทอดองค์ความรู้และเทคโนโลยี ของมาสด้าให้กับวิศวกรชาวไทยทั้ง 4 คน ที่กำลังจะเดินทางไปทำงานร่วมกับมาสด้าในต้นเดือนมิถุนายนนี้ รวมระยะเวลา 4 เดือน ทางมาสด้ามีความยินดีเป็นอย่างยิ่งที่มีส่วนสำคัญในการยกระดับความสามารถของวิศวกรไทย และถือเป็นผู้ผลิตรถยนต์รายแรกที่เข้าร่วมในโครงการนี้\"\n" +
                    "          ดร. อรรชกา สีบุญเรือง รัฐมนตรีว่าการกระทรวงอุตสาหกรรม กล่าวถึงโครงการนี้ว่า โครงการ ODM เป็นการพัฒนาผู้ผลิตชิ้นส่วนยานยนต์ไทยให้มีความรู้ความสามารถ ทักษะในการวิจัยและพัฒนา ตามแนวทางของนโยบายเขตพัฒนาเศรษฐกิจพิเศษในรูปแบบคลัสเตอร์ เพื่อยกระดับอุตสาหกรรมยานยนต์และชิ้นส่วนยานยนต์ไทยไปสู่การเป็นฐานการผลิตยานยนต์ที่ใช้เทคโนโลยีขั้นสูงหรือยานยนต์ในอนาคต อันหมายถึงรถยนต์ที่ขับเคลื่อนด้วยมอเตอร์ไฟฟ้า รวมทั้งรถยนต์เครื่องยนต์สันดาปภายในที่มีการพัฒนาคุณสมบัติ \"สะอาด ประหยัด ปลอดภัย\" ให้ดีขึ้น\n" +
                    "          โครงการพัฒนาผู้ผลิตชิ้นส่วนยานยนต์ไทยไปสู่การผลิตตามรูปแบบของตนเอง หรือ Original Design Manu facturer หรือ ODM นี้ เป็นโครงการนำร่องที่ช่วยเพิ่มขีดความสามารถบุคลากรแบบใหม่ ที่เปิดโอกาสให้วิศวกรจากบริษัทผู้ผลิตชิ้นส่วนไทยได้เข้าไปมีส่วนร่วมในทุกขั้นตอนการทำงานในลักษณะการทำงานจริงกับผู้ผลิตรถยนต์ โดยมาสด้าเปิดโอกาสให้ทีมวิศวกรของผู้ผลิตชิ้นส่วนชาวไทย 3 บริษัทได้แก่ บริษัท ซัมมิท โอโตซีท อินดัสตรี จำกัด บริษัท ซัมมิท โอโต บอดี้ อินดัสตรี จำกัด และบริษัท ไทยซัมมิท โอโตพาร์ท อินดัสตรี จำกัด ได้ไปร่วมงานกับบริษัทมาสด้า มอเตอร์ คอร์ปอเรชั่น ที่กรุงฮิโรชิมาบ้านเกิดของมาสด้า ประเทศญี่ปุ่น";
        }*/

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);



        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText(text);
        getSupportActionBar().setCustomView(viewActionBar, params1);




    }
}
