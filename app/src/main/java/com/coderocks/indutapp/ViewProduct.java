package com.coderocks.indutapp;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

/**
 * Created by watcharatepinkong on 5/6/16 AD.
 */
public class ViewProduct extends AppCompatActivity {

    private Drawer result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product);

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Home");
        SecondaryDrawerItem item2 = (SecondaryDrawerItem) new SecondaryDrawerItem().withName("Category");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




//create the drawer and remember the `Drawer` result object
        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        new SecondaryDrawerItem().withName("Order"),
                        new SecondaryDrawerItem().withName("Tickets"),
                        new SecondaryDrawerItem().withName("Sign In"),
                        new SecondaryDrawerItem().withName("Sign Up")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        result.closeDrawer();
                        switch (position){

                            case 1:
                                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                                startActivity(intent);
                                break;
                            case 2:
                                intent = new Intent(getBaseContext(),CategoryActivity.class);
                                startActivity(intent);
                                break;

                            case 3:
                                intent = new Intent(getBaseContext(),ViewOrder.class);
                                startActivity(intent);
                                break;

                            case 4:
                                intent = new Intent(getBaseContext(),TicketActivity.class);
                                startActivity(intent);
                                break;

                            case 5:
                                intent = new Intent(getBaseContext(),LoginAcivity.class);
                                startActivity(intent);
                                break;
                            case 6:
                                intent = new Intent(getBaseContext(),Signup.class);
                                startActivity(intent);
                                break;
                        }
                        return true;
                    }
                })
                .build();




        Button btnreserve = (Button) findViewById(R.id.btnreserve);
        btnreserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewProduct.this, SignUser.class);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });
    }
}
