package com.coderocks.indutapp;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;



/**
 * Created by watcharatepinkong on 7/19/16 AD.
 */
public class ViewTicket extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewticket);


        Bitmap myBitmap = QRCode1.from("test").bitmap();
        ImageView myImage = (ImageView) findViewById(R.id.qrcode);
        myImage.setImageBitmap(myBitmap);


    }
}
