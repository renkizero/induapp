package com.coderocks.indutapp;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

public class CustomText1 extends TextView {

    public CustomText1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomText1(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomText1(Context context) {
        super(context);
        init();
    }

    private void init() {
        setGravity(Gravity.RIGHT);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "Nunito-Bold.ttf");
        setTypeface(tf);

    }

}