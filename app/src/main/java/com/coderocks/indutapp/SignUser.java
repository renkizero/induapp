package com.coderocks.indutapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

/**
 * Created by watcharatepinkong on 5/7/16 AD.
 */
public class SignUser extends AppCompatActivity {

    private Drawer result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signuser);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Home");
        SecondaryDrawerItem item2 = (SecondaryDrawerItem) new SecondaryDrawerItem().withName("Category");

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        new SecondaryDrawerItem().withName("Order"),
                        new SecondaryDrawerItem().withName("Tickets"),
                        new SecondaryDrawerItem().withName("Sign In"),
                        new SecondaryDrawerItem().withName("Sign Up")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        result.closeDrawer();
                        switch (position){

                            case 1:
                                Intent intent = new Intent(SignUser.this,MainActivity.class);
                                startActivity(intent);
                                break;
                            case 2:
                                intent = new Intent(SignUser.this,CategoryActivity.class);
                                startActivity(intent);
                                break;

                            case 3:
                                intent = new Intent(SignUser.this,ViewOrder.class);
                                startActivity(intent);
                                break;

                            case 4:
                                intent = new Intent(SignUser.this,TicketActivity.class);
                                startActivity(intent);
                                break;

                            case 5:
                                intent = new Intent(SignUser.this,LoginAcivity.class);
                                startActivity(intent);
                                break;
                            case 6:
                                intent = new Intent(SignUser.this,Signup.class);
                                startActivity(intent);
                                break;
                        }
                        return true;
                    }
                })
                .build();

        Button btnregister = (Button) findViewById(R.id.btnregister);
        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUser.this, Signup.class);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
                finish();
            }
        });

        Button btnlogin = (Button) findViewById(R.id.btnlogin);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(SignUser.this,LoginAcivity.class);
                startActivity(intent);
                finish();

            }
        });



    }
}
