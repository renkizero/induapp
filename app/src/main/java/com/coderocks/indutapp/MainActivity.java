package com.coderocks.indutapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity  implements BaseSliderView.OnSliderClickListener{

    private SliderLayout mDemoSlider;
    private Drawer result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withName("Home");
        SecondaryDrawerItem item2 = (SecondaryDrawerItem) new SecondaryDrawerItem().withName("Category");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("slide1",R.drawable.slide4);
        file_maps.put("slide2",R.drawable.slide1);
        file_maps.put("slide3",R.drawable.slide2);
        file_maps.put("slide4", R.drawable.slide3);
        file_maps.put("slide5", R.drawable.slide5);

        for(String name : file_maps.keySet()){
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }



//create the drawer and remember the `Drawer` result object
         result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .addDrawerItems(
                        item1,
                        new DividerDrawerItem(),
                        item2,
                        new SecondaryDrawerItem().withName("Order"),
                        new SecondaryDrawerItem().withName("Tickets"),
                        new SecondaryDrawerItem().withName("Sign In"),
                        new SecondaryDrawerItem().withName("Sign Up")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        Log.d("","====>"+position);
                        result.closeDrawer();
                        switch (position){

                            case 2:
                                Intent intent = new Intent(MainActivity.this,CategoryActivity.class);
                                startActivity(intent);
                                break;

                            case 3:
                                 intent = new Intent(MainActivity.this,ViewOrder.class);
                                startActivity(intent);
                                break;

                            case 4:
                                intent = new Intent(MainActivity.this,TicketActivity.class);
                                startActivity(intent);
                                break;

                            case 5:
                                intent = new Intent(MainActivity.this,LoginAcivity.class);
                                startActivity(intent);
                                break;
                            case 6:
                                intent = new Intent(MainActivity.this,Signup.class);
                                startActivity(intent);
                                break;
                        }
                        return true;
                    }
                })
               .build();

        ImageView img = (ImageView)findViewById(R.id.testbtn);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getBaseContext(), ViewCategory.class);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });




        Button btnc = (Button)findViewById(R.id.categorybtn);

        btnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,CategoryActivity.class);
                startActivity(intent);
            }
        });

        Button ticketbtn = (Button)findViewById(R.id.ticketbtn);

        ticketbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,TicketActivity.class);
                startActivity(intent);
            }
        });


        Button orderbtn = (Button)findViewById(R.id.orderbtn);

        orderbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,OrderDetail.class);
                startActivity(intent);
            }
        });



    }

    @Override
    public void onSliderClick(BaseSliderView slider) {




                    Intent intent = new Intent(getBaseContext(), ViewCategory.class);
                    //String message = "abc";
                    //intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(intent);



    }
}
