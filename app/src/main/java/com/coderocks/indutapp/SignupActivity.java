package com.coderocks.indutapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.system.ErrnoException;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by watcharatepinkong on 5/21/16 AD.
 */
public class SignupActivity extends AppCompatActivity implements Validator.ValidationListener {

   // public static String [] prgmNameList1={"ทั้งหมด","วิทยาศาสตร์","ครอบครัว","การศึกษา","ธุรกิจ","เพลง","สุขภาพ","การสื่อสาร","อาหารและเครื่องดื่ม","แฟชั่น","ศิลปะ","กีฬา","ความบันเทิง","งานอดิเรก","การกุศล"};
   // public static int [] prgmImages1={R.drawable.icon_all,R.drawable.icon_sci,R.drawable.icon_family,R.drawable.icon_education,R.drawable.icon_business,R.drawable.icon_music,R.drawable.icon_health,R.drawable.icon_com,R.drawable.icon_food,R.drawable.icon_fashion,R.drawable.icon_art,R.drawable.icon_sport,R.drawable.icon_movie,R.drawable.icon_hobby,R.drawable.icon_charity};

    ArrayList<String> prgmNameList = new ArrayList<String>();
    //  public static String[] prgmNameList={"ทั้งหมด"};
    public static int[] prgmImages={R.drawable.icon_all,R.drawable.icon_atsi,R.drawable.icon_sme,R.drawable.icon_smebank,R.drawable.icon_thiti,R.drawable.icon_smecn,R.drawable.icon_tma,R.drawable.icon_composite,R.drawable.icon_bsid,R.drawable.icon_subcon};


    public Validator  validator;


    public int s0,s1,s2,s3,s4,s5,s6,s7,s8,s9;

    @NotEmpty
    @Email( message = "Please Check and Enter a valid Email Address")
    public EditText username;

    @Password(min = 6)
    public EditText password;

    @ConfirmPassword
    public EditText confirmpassword;


    public EditText firstname;


    public EditText lastname;

    public EditText phone;


    public int gender;
    public com.github.glomadrian.loadingballs.BallView ballview;
    public ImageView editprofile;

    private File mTempFile;
    private File mTempCropFile;
    private int selectprovince = 0;
    private Uri mCropImageUri;
    private Uri mMediaUrl;

    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int CROP_REQUEST_CODE = 2;
    private static final int GALLERY_REQUEST_CODE = 3;
    private static final int GALLERY_KITKAT_REQUEST_CODE = 4;
    private static final int EDIT_ACCOUNT_REQUEST_CODE = 10;
    private static final int EDIT_AVATAR_REQUEST_CODE = 11;
    private static final int CROP_IMAGE_SIZE = 480;
    RoundedImageView profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


        profile = (RoundedImageView)findViewById(R.id.profile);


        editprofile = (ImageView) findViewById(R.id.editprofile);


        editprofile.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getBaseContext(),Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            100);
                }

                startActivityForResult(getPickImageChooserIntent(), 200);


            }
        });


        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        confirmpassword = (EditText) findViewById(R.id.confirmpassword);

        firstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        phone = (EditText) findViewById(R.id.phone);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }




       ballview = (com.github.glomadrian.loadingballs.BallView)findViewById(R.id.ballView);

        ballview.setVisibility(View.INVISIBLE);


        final ImageView icon_female = (ImageView)findViewById(R.id.icon_female);

        final ImageView icon_male = (ImageView)findViewById(R.id.icom_male);

        final ImageView icon_inde = (ImageView)findViewById(R.id.icon_inde);

        icon_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(icon_female.getTag().equals("off")) {
                    icon_female.setTag("on");
                    icon_female.setImageResource(R.drawable.icon_female_select);
                    icon_male.setImageResource(R.drawable.icon_male);
                    icon_inde.setImageResource(R.drawable.icon_indecision);
                    gender = 1;
                    icon_male.setTag("off");
                    icon_inde.setTag("off");
                }else{

                    icon_female.setTag("off");
                    icon_female.setImageResource(R.drawable.icon_female);
                }

            }
        });



        icon_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(icon_male.getTag().equals("off")) {

                    gender = 2;
                    icon_male.setTag("on");
                    icon_male.setImageResource(R.drawable.icon_male_select);
                    icon_female.setImageResource(R.drawable.icon_female);
                    icon_inde.setImageResource(R.drawable.icon_indecision);

                    icon_female.setTag("off");
                    icon_inde.setTag("off");

                }else{

                    icon_male.setTag("off");
                    icon_male.setImageResource(R.drawable.icon_male);
                }

            }
        });



        icon_inde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(icon_inde.getTag().equals("off")) {
                    icon_inde.setTag("on");
                    icon_inde.setImageResource(R.drawable.icon_indecision_select);

                    icon_female.setImageResource(R.drawable.icon_female);
                    icon_male.setImageResource(R.drawable.icon_male);

                    gender = 3;

                    icon_female.setTag("off");
                    icon_male.setTag("off");

                }else{

                    icon_inde.setTag("off");
                    icon_inde.setImageResource(R.drawable.icon_indecision);
                }

            }
        });




        ImageView signup1 = (ImageView)findViewById(R.id.signup1);
        signup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("","---->"+validator);
                validator.validate();




            }
        });



        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        toolbar.setBackgroundResource(R.drawable.bar_top);



        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText("สมัครสมาชิก");
        getSupportActionBar().setCustomView(viewActionBar, params1);


        final GridView gridView = (GridView) findViewById(R.id.gridView1);



/*        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Item Clicked: " + position, Toast.LENGTH_SHORT).show();

            }
        });*/
      /*  gridView.setAdapter(new CustomAdapter(getBaseContext(), prgmNameList1,prgmImages1));
*/

        prgmNameList.add("ทั้งหมด");
        final com.github.glomadrian.loadingballs.BallView ballview = (com.github.glomadrian.loadingballs.BallView)findViewById(R.id.ballView);

        Ion.with(getBaseContext())
                .load("http://ticketmhee.com/api/index.php/api/getCateAll")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error

                        Log.d("dev","==>"+result);
                        try {
                            if(!result.isJsonNull()){
                                ballview.setVisibility(View.INVISIBLE);
                                JsonArray obj = result.getAsJsonArray("data");
                                // int j = 1;
                                Log.d("dev","==>"+obj.size());
                                for (int i = 0; i < obj.size(); i++) {

                                    if(i>2){
                                        JsonObject news = obj.get(i).getAsJsonObject();
                                        Log.d("dev","==>"+news.get("name").getAsString().toString());
                                        //prgmNameList[] = ""+news.get("name").getAsString().toString();
                                        prgmNameList.add(news.get("name").getAsString().toString());
                                        Log.d("dev","==>");

                                    }
                                }
                                gridView.setAdapter(new CustomAdapter(getBaseContext(), prgmNameList,prgmImages));

                                gridView.notifyAll();
                            }
                        }catch (Exception e1){


                        }



                    }
                });


        validator = new Validator(this);
        validator.setValidationListener(this);

    }


    @Override
    public void onValidationSucceeded() {
       // Toast.makeText(getBaseContext(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();

        ballview.setVisibility(View.VISIBLE);
        Ion.with(getBaseContext())
                .load("POST","http://ticketmhee.com/api/index.php/api/register")
                .setBodyParameter("username", username.getText().toString())
                .setBodyParameter("email", username.getText().toString())
                .setBodyParameter("password", password.getText().toString())
               /* .setBodyParameter("firstname", firstname.getText().toString())
                .setBodyParameter("lastname", lastname.getText().toString())
                .setBodyParameter("birthdate", "")
                .setBodyParameter("phone","")
                .setBodyParameter("gender", "")*/

                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        ballview.setVisibility(View.INVISIBLE);
                        Log.d("dev","result===>"+result.get("success"));
                       // JsonObject obj = result.getAsJsonObject("success");
                        if(!result.isJsonNull()) {
                            if (result.get("success").getAsBoolean()) {

                                Toast.makeText(getBaseContext(), "Register Completed", Toast.LENGTH_LONG).show();
                                finish();

                            }else{

                                CustomDialogClass alert = new CustomDialogClass();
                                alert.showDialog(SignupActivity.this, "Error , Please try agian");



                            }
                        }
                    }
                });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getBaseContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  mCropImageView.setImageUriAsync(mCropImageUri);
        } else {
            Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }


    public Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
    private void selectImageFromCamera() {
        File outputDir = getExternalCacheDir();
        mTempFile = new File(outputDir, "capture");
        mTempFile.deleteOnExit();

        if (mTempFile != null) {
            Uri output = Uri.fromFile(mTempFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, output);
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void selectImageFromGallery() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_REQUEST_CODE);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, GALLERY_KITKAT_REQUEST_CODE);
        }
    }



    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE || requestCode == GALLERY_KITKAT_REQUEST_CODE || requestCode == 200 )
                && resultCode == RESULT_OK) {

          /*  Uri picUri = null;

            if (requestCode == CAMERA_REQUEST_CODE) {

                // Get the Uri for the captured photo.
                picUri = Uri.fromFile(mTempFile);

            } else {

                //final Uri uri = data.getData();
                mMediaUrl = data.getData();
                //Log.d("dev", "Media Uri: " + mMediaUrl);
                // Get the File path from the Uri

                String path = FileUtils.getPath(this, mMediaUrl);

                // Alternatively, use FileUtils.getFile(Context, Uri)
                if (path != null && FileUtils.isLocal(path)) {
                    File file = new File(path);

                    picUri = Uri.fromFile(file);
                }



            }*/


            Uri imageUri = getPickImageResultUri(data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage,
            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            //Log.d("dev", "imageUri Uri: " + requirePermissions);
            if (!requirePermissions) {

                // After getting photo then let's user crop.
                mTempCropFile = new File(getExternalCacheDir(), "tempCrop");
                mTempCropFile.deleteOnExit();

                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                cropIntent.setDataAndType(imageUri, "image/*");
                cropIntent.putExtra("crop", "true");
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("outputX", CROP_IMAGE_SIZE);
                cropIntent.putExtra("outputY", CROP_IMAGE_SIZE);
                cropIntent.putExtra("return-data", false);
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mTempCropFile));
                startActivityForResult(cropIntent, CROP_REQUEST_CODE);
            }

        } else if (requestCode == CROP_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                Bitmap bitmap = BitmapFactory.decodeFile(mTempCropFile.getAbsolutePath());

                uploadProfileImage(bitmap);
            }

            // Delete temporary image cropped.
            if (mTempCropFile != null)
                mTempCropFile.delete();

            if (mTempFile != null)
                mTempFile.delete();

        } else if (requestCode == EDIT_ACCOUNT_REQUEST_CODE || requestCode == EDIT_AVATAR_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                // Show loading.
                // showLoading();

                // loadUserProfile();
            }

        }
    }


    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }
    public Bitmap pic;
    private void uploadProfileImage(Bitmap bitmap) {
        FileOutputStream fos;
        pic = bitmap;
        File file = new File(getExternalCacheDir(), "temp");
        file.deleteOnExit();

        profile.setImageBitmap(bitmap);
       /* RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);

        roundingParams.setRoundAsCircle(true);
        profile.getHierarchy().setRoundingParams(roundingParams);*/

        try {
            fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);

            //HashMap<String, Object> profileData = new HashMap<>();
            //profileData.put("image", file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public class CustomAdapter extends BaseAdapter {

        ArrayList<String>  result;
        Context context;
        int[] imageId;
        private LayoutInflater inflater = null;

        public CustomAdapter(Context mainActivity, ArrayList<String>  prgmNameList, int[] prgmImages) {
            // TODO Auto-generated constructor stub
            result = prgmNameList;
            context = mainActivity;
            imageId = prgmImages;
            inflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return result.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder {
            TextView tv;
            ImageView img;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            final Holder holder = new Holder();
            View rowView;

            rowView = inflater.inflate(R.layout.catelist, null);
            holder.tv = (TextView) rowView.findViewById(R.id.textView1);
            holder.img = (ImageView) rowView.findViewById(R.id.imageView1);

            holder.tv.setTextColor(Color.parseColor("#10579e"));
            holder.tv.setText(result.get(position));
            holder.img.setImageResource(imageId[position]);

            rowView.setOnClickListener(new View.OnClickListener() {
//public static int[] prgmImages={R.drawable.icon_all,R.drawable.icon_atsi,R.drawable.icon_sme,R.drawable.icon_smebank,R.drawable.icon_thiti,R.drawable.icon_smecn,R.drawable.icon_tma,R.drawable.icon_composite,R.drawable.icon_bsid,R.drawable.icon_subcon};

                @Override
                public void onClick(View v) {

                    Log.d("dev","---");

                    if(position == 0){

                        if( s0 == 0){
                            s0 = 1;
                            holder.img.setImageResource(R.drawable.icon_all_select);
                        }else{
                            s0 = 0;
                            holder.img.setImageResource(R.drawable.icon_all);
                        }


                    }else if(position == 1){
                        if( s1 == 0){
                            s1 = 1;
                            holder.img.setImageResource(R.drawable.icon_atsi_select);
                        }else{
                            s1 = 0;
                            holder.img.setImageResource(R.drawable.icon_atsi);
                        }

                    }else if(position == 2){

                        if( s2 == 0){
                            s2 = 1;
                            holder.img.setImageResource(R.drawable.icon_sme_select);
                        }else{
                            s2 = 0;
                            holder.img.setImageResource(R.drawable.icon_sme);
                        }


                    }else if(position == 3){

                        if( s3 == 0){
                            s3 = 1;
                            holder.img.setImageResource(R.drawable.icon_smebank_select);
                        }else{
                            s3 = 0;
                            holder.img.setImageResource(R.drawable.icon_smebank);
                        }
                    }else if(position == 4){
                        if( s4 == 0){
                            s4 = 1;
                            holder.img.setImageResource(R.drawable.icon_thiti_select);
                        }else{
                            s4 = 0;
                            holder.img.setImageResource(R.drawable.icon_thiti);
                        }

                    }else if(position == 5){
                        if( s5 == 0){
                            s5 = 1;
                            holder.img.setImageResource(R.drawable.icon_smecn_select);
                        }else{
                            s5 = 0;
                            holder.img.setImageResource(R.drawable.icon_smecn);
                        }

                    }else if(position == 6){
                        if( s6 == 0){
                            s6 = 1;
                            holder.img.setImageResource(R.drawable.icon_tma_select);
                        }else{
                            s6 = 0;
                            holder.img.setImageResource(R.drawable.icon_tma);
                        }

                    }else if(position == 7){

                        if( s7 == 0){
                            s7 = 1;
                            holder.img.setImageResource(R.drawable.icon_composite_select);
                        }else{
                            s7 = 0;
                            holder.img.setImageResource(R.drawable.icon_composite);
                        }


                    }else if(position == 8){
                        if( s8 == 0){
                            s8 = 1;
                            holder.img.setImageResource(R.drawable.icon_bsid_select);
                        }else{
                            s8 = 0;
                            holder.img.setImageResource(R.drawable.icon_bsid);
                        }


                    }else if(position == 9){
                        if( s9 == 0){
                            s9 = 1;
                            holder.img.setImageResource(R.drawable.icon_subcon_select);
                        }else{
                            s9 = 0;
                            holder.img.setImageResource(R.drawable.icon_subcon);
                        }

                    }


                    // TODO Auto-generated method stub
                    //  Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_LONG).show();
                }
            });

            return rowView;
        }
    }

}
