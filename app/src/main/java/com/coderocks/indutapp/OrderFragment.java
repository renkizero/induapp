package com.coderocks.indutapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

/**
 * Created by watcharatepinkong on 5/16/16 AD.
 */
public class OrderFragment extends Fragment {

    /**
     * Create a new instance of the fragment
     */
    public static OrderFragment newInstance(int index) {
        OrderFragment fragment = new OrderFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        return fragment;
    }

    private String getNullAsEmptyString(JsonElement jsonElement) {
        return jsonElement.isJsonNull() ? "" : jsonElement.getAsString();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home, container, false);


        //   ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Industry Directs");

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayOptions( ((AppCompatActivity)getActivity()).getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = inflater.inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText("ข่าวสาร");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(viewActionBar, params1);


        ArrayList<News> arrayOfUsers = new ArrayList<News>();



        // get api ticketmhee.com/api/index.php/api/getNewsAll

        final UsersAdapter adapter = new UsersAdapter(getActivity(), arrayOfUsers);
        final com.github.glomadrian.loadingballs.BallView ballview = (com.github.glomadrian.loadingballs.BallView)view.findViewById(R.id.ballView);



        Ion.with(getActivity())
                .load("http://ticketmhee.com/api/index.php/api/getNewsAll")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        ballview.setVisibility(View.INVISIBLE);
                        Log.d("dev","==>"+result);

                        JsonArray obj = result.getAsJsonArray("data");

                        for (int i = 0; i < obj.size(); i++) {
                            JsonObject news = obj.get(i).getAsJsonObject();


                            if(!news.get("path").isJsonNull()){
                                News newUser = new News( news.get("title").getAsString(), news.get("date").getAsString(), news.get("time").getAsString(), news.get("path").getAsString(),news.get("id").getAsString());
                                adapter.add(newUser);

                            }else{
                                News newUser = new News( news.get("title").getAsString(), news.get("date").getAsString(), news.get("time").getAsString(),"",news.get("id").getAsString());
                                adapter.add(newUser);
                            }




                        }

                    }
                });




// Create the adapter to convert the array to views


       /*
        newUser = new User("ข่าวเตือนภัย EE  เดือนเมษายน 2559 (1)", "",R.drawable.news2,2);
        adapter.add(newUser);


        newUser = new User("ยกระดับขีดความสามารถผู้ผลิตชิ้นส่วนยานยนต์ไทย", "",R.drawable.news3,3);
        adapter.add(newUser);


        newUser = new User("มาสด้า เปิดบ้านรับวิศวกรไทย พัฒนาชิ้นส่วนยานยนต์สู่ตลาดโลก", "",0,4);
        adapter.add(newUser);*/


// Attach the adapter to a ListView
        ListView listView = (ListView) view.findViewById(R.id.cateList);
        listView.setAdapter(adapter);


      /*  listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(getActivity(), ViewCategory.class);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });*/

        return view;

    }

    public class UsersAdapter extends ArrayAdapter<News> {
        public UsersAdapter(Context context, ArrayList<News> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final News news = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.orderlistitem, parent, false);
            }

            // Lookup view for data population
            //TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            //TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            // Populate the data into the template view using the data object
            //tvName.setText(user.name);
            //tvHome.setText(user.hometown);
            // Return the completed view to render on screen
            return convertView;
        }
    }

    public class News {
        public String title;
        public String date;
        public String time;
        public String image;
        public String id;
        public News(String title, String date, String time, String img , String id) {
            this.title = title;
            this.date = date;
            this.time = time;
            this.image = img;
            this.id = id;
        }


    }

}
