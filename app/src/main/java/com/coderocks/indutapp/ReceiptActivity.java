package com.coderocks.indutapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.NameValuePair;
import com.koushikdutta.ion.Ion;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.omise.android.models.Token;
import co.omise.android.ui.CreditCardActivity;

/**
 * Created by watcharatepinkong on 5/17/16 AD.
 */
public class ReceiptActivity extends AppCompatActivity implements Validator.ValidationListener {


    public String data1;
    public String data2;
    public String data3;
    public String data4;
    public String data5;

/*
    public String id1;
    public String id2;
    public String id3;
    public String id4;
    public String id5;*/

    private int last = 1;
    private int pay = 0;


    TextView type1 ;
    TextView cost1 ;
    TextView qty1  ;
    TextView total1 ;



    TextView type2 ;
    TextView cost2 ;
    TextView qty2  ;
    TextView total2 ;


    TextView type3 ;
    TextView cost3 ;
    TextView qty3  ;
    TextView total3 ;


    TextView type4 ;
    TextView cost4 ;
    TextView qty4  ;
    TextView total4 ;

    TextView type5 ;
    TextView cost5 ;
    TextView qty5  ;
    TextView total5 ;

    ImageView btn_ss;


    String id1;
    String id2;
    String id3;
    String id4;
    String id5;



    String numid1;
    String numid2;
    String numid3;
    String numid4;
    String numid5;



    public static final String EXTRA_TOKEN = "ReceiptActivity.token";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchaselayout1);





        Bundle bundle = getIntent().getExtras();

        // ArrayList<NameValuePair> nameValuePairs = (ArrayList<NameValuePair>) bundle.getSerializable("allorder");

        final String text = bundle.getString("title");
        final String id = bundle.getString("id");
        final Integer image = bundle.getInt("image");
        final String time = bundle.getString("time");

        final String path = bundle.getString("path");

        final String price = bundle.getString("price");
        final String qty = bundle.getString("qty");


        type1 = (TextView)findViewById(R.id.type1);
        cost1 = (TextView)findViewById(R.id.cost1);
        qty1 = (TextView)findViewById(R.id.qty1);
        total1 = (TextView)findViewById(R.id.total1);

        type2 = (TextView)findViewById(R.id.type2);
        cost2 = (TextView)findViewById(R.id.cost2);
        qty2 = (TextView)findViewById(R.id.qty2);
        total2 = (TextView)findViewById(R.id.total2);



        type3 = (TextView)findViewById(R.id.type3);
        cost3 = (TextView)findViewById(R.id.cost3);
        qty3 = (TextView)findViewById(R.id.qty3);
        total3 = (TextView)findViewById(R.id.total3);


        type4 = (TextView)findViewById(R.id.type4);
        cost4 = (TextView)findViewById(R.id.cost4);
        qty4 = (TextView)findViewById(R.id.qty4);
        total4 = (TextView)findViewById(R.id.total4);


        type5 = (TextView)findViewById(R.id.type5);
        cost5 = (TextView)findViewById(R.id.cost5);
        qty5 = (TextView)findViewById(R.id.qty5);
        total5 = (TextView)findViewById(R.id.total5);


        btn_ss = (ImageView)findViewById(R.id.btn_ss);

        type1.setVisibility(View.GONE);
        cost1.setVisibility(View.GONE);
        qty1.setVisibility(View.GONE);
        total1.setVisibility(View.GONE);


        type2.setVisibility(View.GONE);
        cost2.setVisibility(View.GONE);
        qty2.setVisibility(View.GONE);
        total2.setVisibility(View.GONE);


        type3.setVisibility(View.GONE);
        cost3.setVisibility(View.GONE);
        qty3.setVisibility(View.GONE);
        total3.setVisibility(View.GONE);


        type4.setVisibility(View.GONE);
        cost4.setVisibility(View.GONE);
        qty4.setVisibility(View.GONE);
        total4.setVisibility(View.GONE);


        type5.setVisibility(View.GONE);
        cost5.setVisibility(View.GONE);
        qty5.setVisibility(View.GONE);
        total5.setVisibility(View.GONE);


        try {
            data1 = bundle.getString("data1");
            String[] str = data1.split(",");
            id1 = str[3];
            numid1 = str[2];

            showtext(last , str[0] , str[1] , str[2] , str[3]);

        }catch(Exception e){


        }


        try {
            data2 = bundle.getString("data2");
            String[] str = data2.split(",");
            id2 = str[3];
            numid2 = str[2];
            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }



        try {
            data3 = bundle.getString("data3");
            String[] str = data3.split(",");
            numid3 = str[2];
            id3 = str[2];
            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }


        try {
            data4 = bundle.getString("data4");
            String[] str = data4.split(",");
            id4 = str[3];
            numid4 = str[2];
            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }


        try {
            data5 = bundle.getString("data5");
            String[] str = data5.split(",");
            id5 = str[3];
            numid5 = str[2];
            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        toolbar.setBackgroundResource(R.drawable.bar_top);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText(text);
        getSupportActionBar().setCustomView(viewActionBar, params1);




        Ion.with(getBaseContext())
                .load("POST", "http://ticketmhee.com/api/index.php/api/saveticket")
                .setBodyParameter("ticket_id[]", id1)
                .setBodyParameter("ticket_id[]", id2)
                .setBodyParameter("ticket_id[]", id3)
                .setBodyParameter("ticket_id[]", id4)
                .setBodyParameter("ticket_id[]", id5)
                .setBodyParameter("user_id", "")
                .setBodyParameter("ticket_id[]", id1)
                .setBodyParameter("ticket_id[]", id2)
                .setBodyParameter("ticket_id[]", id3)
                .setBodyParameter("ticket_id[]", id4)
                .setBodyParameter("ticket_id[]", id5)

                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error


                    }
                });



      /*  ImageView choosebank = (ImageView)findViewById(R.id.choosebank);


        choosebank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), ChooseBankActivity.class);

                startActivity(intent);
                finish();
            }
        });*/




/*
        for(int i =0;i<=nameValuePairs.size();i++){


        }*/


/*



        eventimg.setImageResource(image);

        TextView qtylabel = (TextView)findViewById(R.id.qty);

        TextView pricelabel = (TextView)findViewById(R.id.price);

        if(price.equals("0")){
            qtylabel.setText(qty);
            pricelabel.setText("FREE");
        }else{
            qtylabel.setText(qty);
            pricelabel.setText(price);
        }

        final TextView qty1 = (TextView)findViewById(R.id.qty);




        ImageView testorder = (ImageView)findViewById(R.id.testpurchase2);

        testorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), ViewPurchaseResultActivity.class);

                intent.putExtra("title",text);
                intent.putExtra("id",id);
                intent.putExtra("image",image);
                intent.putExtra("time",time);
                intent.putExtra("price",price);
                intent.putExtra("qty",qty);
                //String message = "abc";
                //intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
                finish();
            }
        });*/
    }
    public  void showtext(int type , String s1 , String s2 , String s3, String s4 ){
        if(type == 1){

            type1.setText(s1);
            cost1.setText(s2);
            qty1.setText(s3);

            id1 = s4;

            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);

            type1.setVisibility(View.VISIBLE);
            cost1.setVisibility(View.VISIBLE);
            qty1.setVisibility(View.VISIBLE);
            total1.setVisibility(View.VISIBLE);

            total1.setText(""+tmp);

        }else if(type == 2){

            type2.setText(s1);
            cost2.setText(s2);
            qty2.setText(s3);
            // total2.setText(s4);

            id2 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type2.setVisibility(View.VISIBLE);
            cost2.setVisibility(View.VISIBLE);
            qty2.setVisibility(View.VISIBLE);
            total2.setVisibility(View.VISIBLE);
            total2.setText(""+tmp);

        }else if(type == 3){

            type3.setText(s1);
            cost3.setText(s2);
            qty3.setText(s3);
            // total2.setText(s4);

            id3 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type3.setVisibility(View.VISIBLE);
            cost3.setVisibility(View.VISIBLE);
            qty3.setVisibility(View.VISIBLE);
            total3.setVisibility(View.VISIBLE);
            total3.setText(""+tmp);

        }else if(type == 4){

            type4.setText(s1);
            cost4.setText(s2);
            qty4.setText(s3);
            // total2.setText(s4);

            id4 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type4.setVisibility(View.VISIBLE);
            cost4.setVisibility(View.VISIBLE);
            qty4.setVisibility(View.VISIBLE);
            total4.setVisibility(View.VISIBLE);
            total4.setText(""+tmp);

        }else if(type == 5){

            type5.setText(s1);
            cost5.setText(s2);
            qty5.setText(s3);
            // total2.setText(s4);

            id5 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type5.setVisibility(View.VISIBLE);
            cost5.setVisibility(View.VISIBLE);
            qty5.setVisibility(View.VISIBLE);
            total5.setVisibility(View.VISIBLE);
            total5.setText(""+tmp);

        }

        last = type +1;
    }




    private static final String OMISE_PKEY = "pkey_test_52p2h2h1ujsv0pf3ii2";
    private static final int REQUEST_CC = 100;

    private void showCreditCardForm() {
        Intent intent = new Intent(this, CreditCardActivity.class);
        intent.putExtra(CreditCardActivity.EXTRA_PKEY, OMISE_PKEY);
        startActivityForResult(intent, REQUEST_CC);
    }

    @Override
    public void onValidationSucceeded() {

        Log.d("dev","---->onValidationSucceeded");
        /*complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });*/


        if(pay == 1){
            showCreditCardForm();

        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {


        Log.d("dev","---->onValidationFailed");
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getBaseContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}