package com.coderocks.indutapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by watcharatepinkong on 5/22/16 AD.
 */
public class WaitingPaymentActivity extends AppCompatActivity {


    public String data1;
    public String data2;
    public String data3;
    public String data4;
    public String data5;

    private int last = 1;


    public String id1;
    public String id2;
    public String id3;
    public String id4;
    public String id5;


    TextView type1 ;
    TextView cost1 ;
    TextView qty1  ;
    TextView total1 ;



    TextView type2 ;
    TextView cost2 ;
    TextView qty2  ;
    TextView total2 ;


    TextView type3 ;
    TextView cost3 ;
    TextView qty3  ;
    TextView total3 ;


    TextView type4 ;
    TextView cost4 ;
    TextView qty4  ;
    TextView total4 ;

    TextView type5 ;
    TextView cost5 ;
    TextView qty5  ;
    TextView total5 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waitingpayment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Bundle bundle = getIntent().getExtras();

        try {
            data1 = bundle.getString("data1");
            String[] str = data1.split(",");


            showtext(last , str[0] , str[1] , str[2] , str[3]);

        }catch(Exception e){


        }


        try {
            data2 = bundle.getString("data2");
            String[] str = data2.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }



        try {
            data3 = bundle.getString("data3");
            String[] str = data3.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }


        try {
            data4 = bundle.getString("data4");
            String[] str = data4.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }


        try {
            data5 = bundle.getString("data5");
            String[] str = data5.split(",");


            showtext(last , str[0] , str[1] , str[2], str[3]);

        }catch(Exception e){


        }



        toolbar.setBackgroundResource(R.drawable.bar_top);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText("การสั่งซื้อสำเร็จ");
        getSupportActionBar().setCustomView(viewActionBar, params1);

        ImageView verfity = (ImageView)findViewById(R.id.verfity);


        verfity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(getBaseContext(), ConfirmSlipActivity.class);

                startActivity(intent);
                finish();
            }
        });

    }


    public  void showtext(int type , String s1 , String s2 , String s3, String s4 ){
        if(type == 1){

            type1.setText(s1);
            cost1.setText(s2);
            qty1.setText(s3);

            id1 = s4;

            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);

            type1.setVisibility(View.VISIBLE);
            cost1.setVisibility(View.VISIBLE);
            qty1.setVisibility(View.VISIBLE);
            total1.setVisibility(View.VISIBLE);

            total1.setText(""+tmp);

        }else if(type == 2){

            type2.setText(s1);
            cost2.setText(s2);
            qty2.setText(s3);
            // total2.setText(s4);

            id2 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type2.setVisibility(View.VISIBLE);
            cost2.setVisibility(View.VISIBLE);
            qty2.setVisibility(View.VISIBLE);
            total2.setVisibility(View.VISIBLE);
            total2.setText(""+tmp);

        }else if(type == 3){

            type3.setText(s1);
            cost3.setText(s2);
            qty3.setText(s3);
            // total2.setText(s4);

            id3 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type3.setVisibility(View.VISIBLE);
            cost3.setVisibility(View.VISIBLE);
            qty3.setVisibility(View.VISIBLE);
            total3.setVisibility(View.VISIBLE);
            total3.setText(""+tmp);

        }else if(type == 4){

            type4.setText(s1);
            cost4.setText(s2);
            qty4.setText(s3);
            // total2.setText(s4);

            id4 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type4.setVisibility(View.VISIBLE);
            cost4.setVisibility(View.VISIBLE);
            qty4.setVisibility(View.VISIBLE);
            total4.setVisibility(View.VISIBLE);
            total4.setText(""+tmp);

        }else if(type == 5){

            type5.setText(s1);
            cost5.setText(s2);
            qty5.setText(s3);
            // total2.setText(s4);

            id5 = s4;
            int tmp = Integer.parseInt(s2) *  Integer.parseInt(s3);


            type5.setVisibility(View.VISIBLE);
            cost5.setVisibility(View.VISIBLE);
            qty5.setVisibility(View.VISIBLE);
            total5.setVisibility(View.VISIBLE);
            total5.setText(""+tmp);

        }

        last = type +1;
    }

}
