package com.coderocks.indutapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class ViewCategoryActivity  extends AppCompatActivity {

    String[] months = {"มกราคม ", "กุมภาพันธ์ ", "มีนาคม ", "เมษายน ", "พฤษภาคม ", "มิถุนายน ", "กรกฎาคม ",  "สิงหาคม ", "กันยายน ", "ตุลาคม ", "พฤศจิกายน ", "ธันวาคม "};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewcategory);





        Bundle bundle = getIntent().getExtras();
        final String gid = bundle.getString("gid");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
       getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText("Industry Directs");
        getSupportActionBar().setCustomView(viewActionBar, params1);



        ArrayList<User> arrayOfUsers = new ArrayList<User>();


// Create the adapter to convert the array to views
       /* UsersAdapter adapter = new UsersAdapter(getBaseContext(), arrayOfUsers);

        if(gid.equals("0")){
            User newUser = new User("เทคนิคการฉีดพลาสติก พื้นฐาน รุ่นที่ 10", "วันที่ 23-25 พฤษภาคม 2559","","7500",R.drawable.test1,"1");
            adapter.add(newUser);

            newUser = new User("เสกสรรค์ปั้นตลาด เสริมมูลค่าเพิ่ม ด้วยนวัตกรรมบรรจุภัณฑ์", "วันที่ 23-25 พฤษภาคม 2559","","0",R.drawable.test2,"2");
            adapter.add(newUser);
        }else  if(gid.equals("1")){



            User newUser = new User("หลักสูตรการเขียนโปรแกรม PIC ด้วย ภาษาซี และ การใช้งาน Internet of Thing", "วันที่ 26-27 พฤษภาคม 2559","","3800",R.drawable.test5,"3");
            adapter.add(newUser);




            newUser = new User("การพัฒนาศักยภาพแรงงานไทยใน Super Cluster ไฟฟ้าและอิเล็กทรอนิกส์", "วันที่ 23 มิถุนายน 2559","","0",R.drawable.test6,"4");
            adapter.add(newUser);
        }else  if(gid.equals("2")){


            User newUser = new User("ความร่วมมือในการพัฒนาอุตสาหกรรมการผลิตชิ้นส่วนยานยนต์ในกลุ่มประเทศอาเซียน", "วันที่ 11 พฤษภาคม 2559","","0",R.drawable.test7,"5");
            adapter.add(newUser);




            newUser = new User("ศูนย์ทดสอบยานยนต์และชิ้นส่วน ความสำคัญใน Auto Technopolis ของประเทศไทย", "วันที่ 13 พฤษภาคม 2559","","0",R.drawable.test8,"6");
            adapter.add(newUser);






            newUser = new User("การชุบแข็งชิ้นส่วนยานยนต์และเครื่องจักรกล", "วันที่  11 – 12 พฤษภาคม 2559","","4800",R.drawable.test3,"7");
            adapter.add(newUser);

        }








        User   newUser = new User("", "","","0",0,"0");
        adapter.add(newUser);
*/

        final UsersAdapter adapter = new UsersAdapter(getBaseContext(), arrayOfUsers);


        final com.github.glomadrian.loadingballs.BallView ballview = (com.github.glomadrian.loadingballs.BallView)findViewById(R.id.ballView);

        String url = "";
        if(gid.equals("0")){
            url = "http://ticketmhee.com/api/index.php/api/getEventAll";

        }else{

            url = "http://ticketmhee.com/api/index.php/api/getEventByID";
        }
        Ion.with(getBaseContext())
                .load(url)
                .setBodyParameter("id",gid)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error

                        Log.d("dev","==>"+result);
                        ballview.setVisibility(View.INVISIBLE);
                        JsonArray obj = result.getAsJsonArray("data");
                        try {
                            for (int i = 0; i < obj.size(); i++) {
                                JsonObject news = obj.get(i).getAsJsonObject();
                                //  News newUser = new News( news.get("title").getAsString(), news.get("date").getAsString(), news.get("time").getAsString(), R.drawable.news2,2);
                                // adapter.add(newUser);

                                Log.d("==>","===== >"+news);

                                String image = "";
                                try {

                                    image=news.get("path").getAsString();

                                    User newUser = new User(news.get("name").getAsString(), news.get("start_date").getAsString(),"","7500",R.drawable.test1,news.get("id").getAsString(),image);
                                    adapter.add(newUser);

                                }catch (NullPointerException ex){

                                    image = "";


                                    Log.d("==>","===== >"+news.get("name").getAsString());
                                    Log.d("==>","===== >"+news.get("start_date").getAsString());
                                    User newUser = new User(news.get("name").getAsString(), news.get("start_date").getAsString(),"","7500",R.drawable.test1,news.get("id").getAsString(),image);
                                    adapter.add(newUser);

                                }


                            }
                        }catch (Exception e1)
                        {


                        }


                    }
                });


// Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.cateList);
        listView.setAdapter(adapter);



// Attach the adapter to a ListView
      /*  ListView listView = (ListView) findViewById(R.id.cateList);
        listView.setAdapter(adapter);*/


    }

/*
    public class UsersAdapter extends ArrayAdapter<User> {
        public UsersAdapter(Context context, ArrayList<User> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final User user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.homelistitem, parent, false);
            }
            ImageView img = (ImageView) convertView.findViewById(R.id.eventimg);
            if(user.image > 0){
                img.setImageResource(user.image);

            }else {
                img.setImageResource(R.drawable.event_photo);
            }

            ImageView ticketbtn = (ImageView) convertView.findViewById(R.id.ticketbtn);

            ticketbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(getBaseContext(), ViewCategoryDetail.class);
                    intent.putExtra("title",user.name);
                    intent.putExtra("image",user.image);
                    intent.putExtra("id",user.id);
                    intent.putExtra("time",user.time);
                    intent.putExtra("price",user.price);
                    //String message = "abc";
                    //intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(intent);

                }
            });

            TextView titletxt = (TextView) convertView.findViewById(R.id.titletxt);

            titletxt.setText(user.name);
            // Lookup view for data population
            //TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
            //TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            // Populate the data into the template view using the data object
            //tvName.setText(user.name);
            //tvHome.setText(user.hometown);
            // Return the completed view to render on screen
            return convertView;
        }
    }*/


    public class UsersAdapter extends ArrayAdapter<User> {
        public UsersAdapter(Context context, ArrayList<User> users) {
            super(context, 0, users);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            final User user = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.homelistitem, parent, false);
            }
            ImageView img = (ImageView) convertView.findViewById(R.id.slip);
            if(!user.imageurl.equals("")){
                // img.setImageResource(user.imageurl);
                Ion.with(getContext())
                        .load(user.imageurl)
                        .withBitmap()
                        .placeholder(R.drawable.event_photo)
                        .error(R.drawable.event_photo)
                        .intoImageView(img);
            }else {
                img.setImageResource(R.drawable.event_photo);
            }
            ImageView ticketbtn = (ImageView) convertView.findViewById(R.id.ticketbtn);

            ticketbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("","user.id"+user.id);
                    Intent intent = new Intent(getBaseContext(), ViewCategoryDetail.class);
                    intent.putExtra("title",user.name);
                    intent.putExtra("image",user.image);
                    intent.putExtra("id",user.id);
                    intent.putExtra("time",user.time);
                    intent.putExtra("price",user.price);

                    //String message = "abc";
                    //intent.putExtra(EXTRA_MESSAGE, message);
                    startActivity(intent);

                }
            });

            // Lookup view for data population
            TextView titletxt = (TextView) convertView.findViewById(R.id.titletxt);
            TextView timetxt = (TextView) convertView.findViewById(R.id.timetxt);
            timetxt.setText(user.time);
            titletxt.setText(user.name);

            TextView sdate = (TextView) convertView.findViewById(R.id.sdate);
            TextView smonth = (TextView) convertView.findViewById(R.id.smonth);
            TextView syear = (TextView) convertView.findViewById(R.id.syear);

            try{
                String[] str = user.time.split("-");
                sdate.setText(str[2]);
                smonth.setText(months[Integer.parseInt(str[1])]);
                syear.setText(str[0]);


            }catch (Exception e){


            }
            //TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
            // Populate the data into the template view using the data object
            //tvName.setText(user.name);
            //tvHome.setText(user.hometown);
            // Return the completed view to render on screen
            return convertView;
        }
    }

    /*public class User {
        public String name;
        public String hometown;
        public String time;
        public String detail;
        public String price;
        public Integer image;
        public String id;
        public User(String name, String time, String detail, String price, Integer img,String id) {
            this.name = name;
            this.time = time;
            this.detail = detail;
            this.price = price;
            this.image = img;
            this.id = id;
        }
    }*/

    public class User {
        public String name;
        public String hometown;
        public String time;
        public String detail;
        public String price;
        public Integer image;
        public String id;
        public String imageurl;
        public User(String name, String time, String detail, String price, Integer img,String id,String imageurl) {
            this.name = name;
            this.time = time;
            this.detail = detail;
            this.price = price;
            this.image = img;
            this.id = id;
            this.imageurl = imageurl;
        }
    }
}