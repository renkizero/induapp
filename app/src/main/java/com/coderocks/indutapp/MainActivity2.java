package com.coderocks.indutapp;

import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TabHost;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.mikepenz.materialdrawer.Drawer;

import java.util.ArrayList;

/**
 * Created by watcharatepinkong on 5/16/16 AD.
 */
public class MainActivity2  extends AppCompatActivity {


    private SettingFragment currentFragment4;
    private OrderFragment currentFragment3;
    private NewsFragment currentFragment2;
    private CategoryFrament currentFragment1;
    private HomeFragment currentFragment;
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
    private FragmentManager fragmentManager = getFragmentManager();
    private AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintab);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Industry Directs");
        toolbar.setBackgroundResource(R.drawable.bar_top);

        final AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

// Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("หน้าหลัก", R.drawable.icon_home);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("หมวดหมู่", R.drawable.icon_categaries);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("ข่าวสาร", R.drawable.icon_news);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("คำสั่งซื้อ", R.drawable.icon_order);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("บัตรเข้างาน", R.drawable.icon_ticket);


// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

// Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#10589e"));

// Disable the translation inside the CoordinatorLayout
     //   bottomNavigation.setBehaviorTranslationEnabled(false);

// Change colors
       bottomNavigation.setAccentColor(Color.parseColor("#f3bc07"));
       bottomNavigation.setInactiveColor(Color.parseColor("#FFFFFF"));

// Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

// Force the titles to be displayed (against Material Design guidelines!)
        bottomNavigation.setForceTitlesDisplay(true);

// Use colored navigation with circle reveal effect
        //bottomNavigation.setColored(true);

// Set current item programmatically
        bottomNavigation.setCurrentItem(0);


        currentFragment = HomeFragment.newInstance(0);
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                .replace(R.id.fragment_container, currentFragment)
                .commit();

// Set listener
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                if (!wasSelected) {
                    if(position==0){
                        currentFragment = HomeFragment.newInstance(position);
                        fragmentManager.beginTransaction()
                                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                                .replace(R.id.fragment_container, currentFragment)
                                .commit();
                    }else if(position==1){


                        currentFragment1 = CategoryFrament.newInstance(position);
                        fragmentManager.beginTransaction()
                                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                                .replace(R.id.fragment_container, currentFragment1)
                                .commit();

                    }else if(position==2){


                        currentFragment2 = NewsFragment.newInstance(position);
                        fragmentManager.beginTransaction()
                                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                                .replace(R.id.fragment_container, currentFragment2)
                                .commit();

                    }else if(position==3){


                        currentFragment3 = OrderFragment.newInstance(position);
                        fragmentManager.beginTransaction()
                                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                                .replace(R.id.fragment_container, currentFragment3)
                                .commit();

                    }else if(position==4){


                        currentFragment4 = SettingFragment.newInstance(position);
                        fragmentManager.beginTransaction()
                                .setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
                                .replace(R.id.fragment_container, currentFragment4)
                                .commit();

                    }

                }

            }
        });

        Log.d("dev","===>");
    }
}
