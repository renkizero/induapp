package com.coderocks.indutapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by watcharatepinkong on 5/21/16 AD.
 */
public class ChooseBankActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosebank);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        toolbar.setBackgroundResource(R.drawable.bar_top);

        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayOptions( getSupportActionBar().DISPLAY_SHOW_CUSTOM);
        // ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(R.layout.abs_layout);
        View viewActionBar = getLayoutInflater().inflate(R.layout.abs_layout, null);
        ActionBar.LayoutParams params1 = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);


        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.mytext);
        textviewTitle.setText("เลือกธนาคาร");
        getSupportActionBar().setCustomView(viewActionBar, params1);


        ImageView choosebank1 = (ImageView)findViewById(R.id.choosebank1);
        ImageView choosebank2 = (ImageView)findViewById(R.id.choosebank2);
        ImageView choosebank3 = (ImageView)findViewById(R.id.choosebank3);
        ImageView choosebank4 = (ImageView)findViewById(R.id.choosebank4);
        ImageView choosebank5 = (ImageView)findViewById(R.id.choosebank5);
        ImageView choosebank6 = (ImageView)findViewById(R.id.choosebank6);
        ImageView choosebank7 = (ImageView)findViewById(R.id.choosebank7);


        choosebank1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","1");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });


        choosebank2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","2");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });

        choosebank3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","3");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });

        choosebank4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","4");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });

        choosebank5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","5");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });

        choosebank6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","6");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });


        choosebank7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra("result","7");
                setResult(Activity.RESULT_OK,returnIntent);
                finish();

            }
        });


    }
}
